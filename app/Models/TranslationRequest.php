<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Facades\Cookie;

class TranslationRequest extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $appends = ['access'];

    public static array $data = ['name', 'phone', 'translation_id'];

    public function translation() : HasOne
    {
        return $this->hasOne(Translation::class, 'id', 'translation_id');
    }

    public function getAccessAttribute() : bool
    {
        $user = User::query()->where('id', Cookie::get('tid'))->first();

        if (!$user)
            return false;

        return $user->translations()->where('translation_id', $this->translation_id)->exists();
    }
}
