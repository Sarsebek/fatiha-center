<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $timestamps = false;

    protected $attributes = [
        'photo_url' => ' ',
        'gender' => ' ',
        'phone' => ' ',
        'kaspi' => ' ',
        'email' => ' ',
        'info' => ' ',
        'description' => ' ',
        'birthday' => ' ',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'first_name',
        'username',
        'auth_date',
        'hash',
        'email',
        'password',
        'photo_url',
        'gender',
        'phone',
        'type',
        'info',
        'kaspi',
        'description',
        'birthday',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function translations() : BelongsToMany
    {
        return $this->belongsToMany(Translation::class, 'user_translation');
    }

}
