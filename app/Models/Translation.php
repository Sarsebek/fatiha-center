<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cookie;

class Translation extends Model
{
    use HasFactory;

    protected $guarded = [];

    public static array $data = ['name', 'description', 'link', 'price', 'date', 'time', 'link', 'language'];

    protected $appends = ['access'];

    public function getAccessAttribute() : bool
    {
        $user = User::query()->where('id', Cookie::get('tid'))->first();

        if (!$user)
            return false;

        return $user->translations()->where('translation_id', $this->id)->exists();
    }
}
