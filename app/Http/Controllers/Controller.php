<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $controller_name = false;

    public function __construct(){

        $this->controller_name = $this->controllerName();
        view()->share('controller_name', $this->controller_name);

        if (request('id'))
        {
            Cookie::queue('tid', request()->get('id'), 3600);

            if (!User::query()->where('id', request()->get('id'))->exists()) {

                $user = new User();

                $user->fill(
                    [
                        'id' => request()->get('id'),
                        'first_name' => request()->get('first_name'),
                        'username' => request()->get('username'),
                        'auth_date' => request()->get('auth_date'),
                        'hash' => request()->get('hash'),
                    ]
                );
                $user->save();
            }

        }
    }

    protected function controllerName()
    {
        if ($this->controller_name === false && !app()->runningInConsole()) {
            $str = Route::current()->getName();
            $this->controller_name = $str;
        }

        return $this->controller_name;
    }
}
