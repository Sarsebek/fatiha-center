<?php

namespace App\Http\Controllers;

use App\Models\Translation as Model;
use Illuminate\View\View;

class TranslationController extends Controller
{

    public function index(): View
    {
        $translations = Model::query()->get();

        return view($this->controllerName(),compact(
            'translations',
        ));
    }

    public function show(Model $item): View
    {
        return view($this->controllerName(),compact(
            'item',
        ));
    }



}
