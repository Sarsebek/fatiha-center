<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\MediaHelper;
use App\Http\Controllers\Controller;
use App\Models\Translation as Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;

class TranslationController extends Controller
{
    public string $title = 'Трансляции';
    public string $namespace_index = 'admin.translation.index';
    public string $namespace_store = 'admin.translation.store';
    public string $namespace_create = 'admin.translation.create';
    public string $namespace_edit = 'admin.translation.edit';
    public string $namespace_update = 'admin.translation.update';
    public string $namespace_destroy = 'admin.translation.destroy';

    public function index(): View
    {
        $translations = Model::query()->get();

        return view($this->controllerName(),[
            'items' => $translations,
            'title' => $this->title,
            'namespace_create' => $this->namespace_create,
            'namespace_edit' => $this->namespace_edit,
            'namespace_destroy' => $this->namespace_destroy
        ]);
    }

    public function create(): View
    {

        $model = new Model();

        return view($this->controllerName(),[
            'item' => $model,
            'title' => $this->title,
            'namespace_index' => $this->namespace_index,
            'namespace_store' => $this->namespace_store,
            'namespace_create' => $this->namespace_create,
            'namespace_edit' => $this->namespace_edit,
        ]);
    }

    public function store(Request $request)
    {
        $model = new Model($request->only(Model::$data));

        $model->price = (bool)$request->price;
        $model->image = MediaHelper::uploadFile($request->file('image'))['path'];
        $model->background = MediaHelper::uploadFile($request->file('background'))['path'];

        if (!$model->save())
            return Redirect::back()->with('warning', __('messages.warning'));

        return redirect()->route($this->namespace_index)->with('success', __('messages.added'));
    }

    public function edit(Model $model): View
    {

        return view($this->controllerName(),[
            'item' => $model,
            'title' => $this->title,
            'namespace_index' => $this->namespace_index,
            'namespace_update' => $this->namespace_update,
            'namespace_create' => $this->namespace_create,
            'namespace_edit' => $this->namespace_edit,
        ]);
    }

    public function update(Model $model, Request $request): RedirectResponse
    {
        $model->fill($request->only(Model::$data));

        $model->price = (bool)$request->price;

        if ($request->file('image')) {
            Storage::delete($model->image);
            $model->image = MediaHelper::uploadFile($request->file('image'))['path'];
        }
        if ($request->file('background')){
            Storage::delete($model->background);
            $model->background = MediaHelper::uploadFile($request->file('background'))['path'];
        }

        if (!$model->save())
            return redirect()->back()->with('warning', __('messages.warning'));

        return redirect()->route($this->namespace_index)->with('success', __('messages.updated'));
    }

    public function destroy(Model $model): RedirectResponse
    {
        Storage::delete($model->image);
        Storage::delete($model->background);

        if (!$model->delete())
            return redirect()->back()->with('warning', __('messages.warning'));

        return redirect()->route($this->namespace_index)->with('success', __('messages.deleted'));
    }

}
