<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Translation;
use App\Models\TranslationRequest as Model;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;

class TranslationRequestController extends Controller
{

    public string $title = 'Заявки';
    public string $namespace_index = 'admin.translation_request.index';
    public string $namespace_show = 'admin.translation_request.show';
    public string $namespace_destroy = 'admin.translation_request.destroy';

    public function index(): View
    {
        $translations = Model::query()->with('translation')->get();

        return view($this->controllerName(),[
            'items' => $translations,
            'title' => $this->title,
            'namespace_show' => $this->namespace_show,
            'namespace_destroy' => $this->namespace_destroy
        ]);
    }

    public function show(Model $model): View
    {
        return view($this->controllerName(),[
            'item' => $model->load('translation'),
            'title' => $this->title,
            'namespace_index' => $this->namespace_index,
        ]);
    }

    public function store(Request $request): JsonResponse
    {
        $user = User::query()->where('id', Cookie::get('tid'))->first();

        if (!$user)
            return response()->json(['status' => 'Unauthorized', 'code' => 401], 401);

        $username = $user->{'username'};

        $model = new Model([
            'name' => $request->name,
            'phone' => $request->phone,
            'username' => $username,
            'translation_id' => $request->translation_id,
        ]);

        $model->save();

        return response()->json(['status' => 'success', 'code' => 200]);
    }

    public function access(Translation $translation, $username)
    {
        $user = User::query()->where('username', $username)->with('translations')->first();

        $user->translations()->sync([$translation->id]);

        return redirect()->back();
    }

    public function destroy(Model $model): RedirectResponse
    {
        if (!$model->delete())
            return redirect()->back()->with('warning', __('messages.warning'));

        return redirect()->route($this->namespace_index)->with('success', __('messages.deleted'));
    }
}
