<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\RedirectController;
use App\Http\Controllers\Admin\TranslationController;
use App\Http\Controllers\TranslationController as UserTranslationController;
use App\Http\Controllers\Admin\TranslationRequestController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth:sanctum'], function ($router) {
    Route::get('/', [RedirectController::class, 'index']);

    Route::controller(TranslationController::class)->group(function (){
        Route::group(['prefix' => 'translation'], function (){
            Route::get('/', 'index')->name('admin.translation.index');
            Route::get('/create', 'create')->name('admin.translation.create');
            Route::post('/', 'store')->name('admin.translation.store');
            Route::get('/edit/{model}', 'edit')->name('admin.translation.edit');
            Route::post('/update/{model}', 'update')->name('admin.translation.update');
            Route::delete('/destroy/{model}', 'destroy')->name('admin.translation.destroy');
        });
    });

    Route::controller(TranslationRequestController::class)->group(function (){
        Route::group(['prefix' => 'translation_request'], function (){
            Route::get('/', 'index')->name('admin.translation_request.index');
            Route::get('/show', 'show')->name('admin.translation_request.show');
            Route::delete('/destroy/{model}', 'destroy')->name('admin.translation_request.destroy');
            Route::get('/access/{translation}/{username}', 'access')->name('admin.translation_request.access');
        });
    });
});
Route::post('admin/translation_request', [TranslationRequestController::class, 'store'])->name('admin.translation_request.store');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::controller(UserTranslationController::class)->group(function () {
    Route::get('/', 'index')->name('site.translation.index');
    Route::get('/{item}', 'show')->name('site.translation.show');
});