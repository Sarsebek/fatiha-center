
    var cache = {
        qv: {}
    };



    vueData.successAdded = false;
    vueData.searchExampleInterval = '';
    vueData.qv = {
        // type: '',

        show: false,
        orderModal: false,

        hasPrev: false,
        hasNext: false,

        persists: false,

        product: {
            url: '',
            id: '',
            name: '',
            alias: '',
            images: '',
            is_price_published: false,
            old_price_client: '',
            price_client: '',
            aval: '',
            type: '',
            brand: '',
            brand_path: '',
            model: '',
            part_number: '',
            article: '',
            catAlias: '',
            product: '',
            parent: '',
            parentId: '',
            parentAlias: '',
            vendor: '',
            status: '',
            gpl: '',
            exchangeRateGeneral: '',
            is_promo: false,
            is_promo_discount: false,
            price_alfakz: '',
            support_price: '',
            support_info: '',
            is_with_support: '',
            isSupportNeeded: '',
        },
        productQuickCode: '',

        loading: false,

        successAdded: false,
    };

    Vue.component('modalboxOrder', {
        template: '#modalbox-order',

        props: ['modalOverlay', 'show', 'orderFormStatus'],

        data: function () {
            return {};
        },
        methods: {
            closeModalbox: function() {
                this.modalOverlay = false;
                this.orderFormStatus = 'not send';
                this.show = false;

                if (this.$parent.qv.persists) {
                    this.$parent.qv.persists = false;
                    this.$parent.qv.show = true;
                }
            },
        },
        directives: {
            clickOutside: {
                bind: function() {

                    // this.event = event => this.vm.$emit(this.expression, event);
                    this.event = function (event) {
                        return this.vm.$emit(this.expression, event);
                    }.bind(this);

                    this.el.addEventListener('click', function (event) {
                        event.stopPropagation();
                    });
                    document.body.addEventListener('click', this.event);
                },
                unbind: function() {
                    this.el.removeEventListener('click', function (event) {
                        event.stopPropagation();
                    });
                    document.body.removeEventListener('click', this.event);
                },
            }
        },
        events: {
            closeModalboxEvent: function() {
                this.closeModalbox();
            },
        }
    });
    Vue.component('modalboxSimple', {
        template: '#modalbox-simple',

        // props: ['show', 'modalOverlay', 'orderFormStatus'],
        props: ['show', 'modalOverlay'],

        data: function () {
            return {};
        },
        methods: {
            closeModalbox: function() {
                this.modalOverlay = false;
                // this.orderFormStatus = 'not send';
                this.show = false;
                //
                // if (this.$parent.qv.persists) {
                //     this.$parent.qv.persists = false;
                //     this.$parent.qv.show = true;
                // }
            }
        },
        directives: {
            clickOutside: {
                bind: function() {

                    // this.event = event => this.vm.$emit(this.expression, event);
                    this.event = function (event) {
                        return this.vm.$emit(this.expression, event);
                    }.bind(this);


                    this.el.addEventListener('click', function (event) {
                        event.stopPropagation();
                    });
                    document.body.addEventListener('click', this.event);
                },
                unbind: function() {
                    this.el.removeEventListener('click', function (event) {
                        event.stopPropagation();
                    });
                    document.body.removeEventListener('click', this.event);
                },
            }
        },
        events: {
            closeModalboxEvent: function() {
                this.closeModalbox();
            },
        }
    });
    Vue.component('enabledFilters', {
        template: '#enabledFilters',

        props: ['query', 'filtersOn', 'betweenFiltersOn', 'priceFromOn', 'priceToOn', 'filterLabels', 'textOn', 'searchText', 'currentFilters'],

        methods: {
            removeAvalFilter: function (priceAlias) {
                vm.query.nalichie = false;

                // vm.ajaxDbRequestFunc();
            },
            removeWithPriceFilter: function (priceAlias) {
                vm.query.s_tsenoi = true;

                // vm.ajaxDbRequestFunc();
            },
            removePriceFilter: function (priceAlias) {
                vm.query[priceAlias] = [];

                // vm.ajaxDbRequestFunc();
            },
            removeTextFilter: function () {
                vm.query['text'] = '';

                // vm.ajaxDbRequestFunc();
            },
            removeFilter: function (id, filterObj) {
                filterObj.$remove(parseInt(id));
                filterObj.$remove(id);
                // vm.ajaxDbRequestFunc();
            },
            resetBetweenFilter: function (filterObj, alias) {
                Vue.set(filterObj, 'ot', this.$parent.query.df.between[alias].min); //reset current filter to min value
                Vue.set(filterObj, 'do', this.$parent.query.df.between[alias].max); //reset current filter to max value

                // vm.ajaxDbRequestFunc();
            },
        },

        // computed: {
        //     filtersOn: function () {
        //         if ((this.query.nalichie && this.query.nalichie === true) ||
        //             this.priceFromOn ||
        //             this.priceToOn ||
        //             (this.query.typeIds && this.query.typeIds.length > 0) ||
        //             (this.query.brend && this.query.brend.length > 0)
        //         ) {
        //             return true;
        //         }

        //         return false;
        //     },
        //     priceFromOn: function () {
        //         return (this.query.tsena_ot && this.query.tsena_ot instanceof Array === false) ? true : false;
        //     },
        //     priceToOn: function () {
        //         return (this.query.tsena_do && this.query.tsena_do instanceof Array === false) ? true : false;
        //     }
        // },

        filters: {
            ifIn: function (obj, ids) {
                var newObj = [];
                for (var i = 0; i <= obj.length - 1; i++) {
                    if ( ids && (Object.values(ids).includes(obj[i].id) || Object.values(ids).includes(parseInt(obj[i].id)) || ids == obj[i].id )) {
                        newObj.push(obj[i]);
                    }
                }
                return newObj;
            }
        },
    });
    Vue.component('star-rating', {

        props: {
            'name': String,
            'value': null,
            'id': String,
            'disabled': Boolean,
            'required': Boolean,
            'width': 19,
            'height': 16
        },

        template:
            '<div class="star-rating"><label class="star-rating__star" v-for="rating in ratings" :class="{\'is-selected\': ((value >= rating) &&\
             value != null), \'is-disabled\': disabled}" v-on:click="set(rating)" v-on:mouseover="star_over(rating)" v-on:mouseout="star_out"><input class="star-rating star-rating__checkbox" type="radio" :value="rating" :name="name" v-model="value" :disabled="disabled" >\
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" :width="width" :height="height" viewBox="0 0 19 16">\
            <path d="M14.18 16c0.181-0.012 0.346-0.074 0.483-0.172 0.179-0.1 0.301-0.289 0.307-0.507 0.008-0.047 0.012-0.101\
             0.012-0.156s-0.004-0.108-0.013-0.161l-1.369-4.744 4-3.38c0.163-0.13 0.268-0.327 0.27-0.55 0.001-0.013 0.001-0.028 0.001-0.043 0-0.208-0.080-0.397-0.212-0.538-0.127-0.129-0.304-0.21-0.5-0.21-0.004 0-0.007 0-0.011 0l-4.809-0.34-2.16-4.76c-0.082-0.26-0.32-0.445-0.602-0.445-0.027 0-0.054 0.002-0.081 0.005-0.020-0.003-0.047-0.005-0.075-0.005-0.282 0-0.52 0.185-0.601 0.44l-2.161 4.764-4.82 0.34c-0.319 0.004-0.588 0.215-0.679 0.505-0.008 0.046-0.012 0.092-0.012 0.14s0.004 0.094 0.011 0.14c-0.002 0.009-0.003 0.026-0.003 0.043 0 0.209 0.107 0.394 0.27 0.501l4.002 3.421-1.37 4.71v0.17c0.029 0.261 0.154 0.488 0.339 0.649 0.119 0.074 0.258 0.124 0.407 0.141 0.169-0.014 0.318-0.065 0.45-0.142l4.245-2.727 4.27 2.77c0.098 0.088 0.229 0.141 0.372 0.141 0.013 0 0.027-0 0.040-0.001z"></path></svg></label></div>',
        /*
         * Initial state of the component's data.
         */
        data: function() {
            return {
                temp_value: null,
                ratings: [1, 2, 3, 4, 5]
            };
        },

        methods: {
            /*
             * Behaviour of the stars on mouseover.
             */
            star_over: function(index) {
                var self = this;

                if (!this.disabled) {
                    this.temp_value = this.value;
                    return this.value = index;
                }

            },

            /*
             * Behaviour of the stars on mouseout.
             */
            star_out: function() {
                var self = this;
                if (!this.disabled) {
                    return this.value = this.temp_value;
                }
            },

            /*
             * Set the rating.
             */
            set: function(value) {
                var self = this;

                if (!this.disabled) {
                    this.temp_value = value;
                    vm.reviewButtonDisabled();
                    return this.value = value;
                }
            }
        }
    });

    Vue.directive('roundValOnChange', {
        params: ['obj'],

        twoWay: true,
        bind: function () {
            var vm = this,
                params = this.params,
                el = vm.el,
                $el = $(vm.el),
                modifiers = vm.modifiers;

            $el.on('change', function () {
                if (modifiers.floor) {
                    var roundedVal = Math.floor(el.value.replace(',', '.'));

                    if (roundedVal > parseInt(params.obj.max)) {
                        roundedVal = parseInt(params.obj.ot);
                    }

                    vm.set(roundedVal);

                    $el.val(roundedVal);
                } else if (modifiers.ceil) {
                    var roundedVal = Math.ceil(el.value.replace(',', '.'));

                    if (roundedVal < parseInt(params.obj.min)) {
                        roundedVal = parseInt(params.obj.do);
                    }

                    vm.set(roundedVal);

                    $el.val(roundedVal);
                }
            });
        }
    });
    Vue.directive('sliderRange', {
        params: ['min', 'max', 'valFrom', 'valTo', 'dfObj'],

        bind: function () {
            var $el = $(this.el),
                vm = this.vm,
                params = this.params,
                $inputFrom = $el.closest('.filter_item').find('input.input_between.from'),
                $inputTo = $el.closest('.filter_item').find('input.input_between.to'),
                min = parseInt(params.min),
                max = parseInt(params.max),
                valFrom = parseInt(params.valFrom),
                valTo = parseInt(params.valTo);

            $el.slider({
                range: true,
                // min: min,
                // max: max,
                // values: [ valFrom, valTo ],
                animate: 'fast',
                slide: function ( event, ui ) {
                    $inputFrom.val(ui.values[ 0 ]);
                    $inputTo.val(ui.values[ 1 ]);
                },
                stop: function ( event, ui ) {
                    Vue.set( params.dfObj, 'ot', ui.values[ 0 ] );
                    Vue.set( params.dfObj, 'do', ui.values[ 1 ] );

                    vm.ajaxDbRequestFunc();
                },
            });

            $el.slider( 'option', 'min', min );
            $el.slider( 'option', 'max', max );
            $el.slider( 'option', 'values', [ valFrom, valTo ] );

            // if (valFrom == valTo) {
            //     $el.slider( 'option', 'max', max + 1 );
            //     $el.slider( 'option', 'values', [ valFrom, valTo + 1 ] );

            //     $el.slider( 'disable' );
            // } else {
            //     $el.slider( 'option', 'max', max );
            //     $el.slider( 'option', 'values', [ valFrom, valTo ] );
            // }
        }
    });
    Vue.directive('filtersToggler', {
        bind: function () {

            $(this.el).on('click', '.filter_name', function () {
                let $filters = $(this).closest('.filter_wrapper').find('.filter_items').first(),
                    $trigger_icon = $(this).find('.fa');

                $.when($filters.slideToggle()).done(function () {
                    if ($trigger_icon.hasClass('fa-chevron-down')) {
                        $trigger_icon.removeClass('fa-chevron-down').addClass('fa-chevron-up');
                    } else if ($trigger_icon.hasClass('fa-chevron-up')) {
                        $trigger_icon.removeClass('fa-chevron-up').addClass('fa-chevron-down');
                    }
                });
            });
        },
    });

    Vue.filter('numericFormat', {
        // read: function (val) {},
        write: function (val, oldVal) {
            if ((val.length === 1 && parseInt(val) === 0) || !$.isNumeric(val) || ($.isNumeric(val) && val < 0)) {
                return oldVal;
            }

            return val;
        }
    });
    Vue.filter('priceFormat', {
        read: function (val) {
            if (!val) {
                return '0';
            }

            val = val.toString().replace(/\s/g, '');

            return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
        },
        write: function (val, oldVal, type) {
            var val = val.toString().replace(/\s/g, ''),
                oldVal = oldVal.toString().replace(/\s/g, '');

            if (!val && val.length === 0) {
                return [];
            }

            if ((val.length === 1 && parseInt(val) === 0) || !$.isNumeric(val) || ($.isNumeric(val) && val < 0)) {
                return oldVal ? oldVal : (type == 'from' ? this.minPrice : this.maxPrice);
            }

            if (type == 'from' && parseInt(val) > parseInt(this.maxPrice)) {
                return this.minPrice;
            } else if (type == 'to' && parseInt(val) < parseInt(this.minPrice)) {
                return this.maxPrice;
            }

            return val;
        }
    });

    Vue.directive('autoSelect', {
        bind: function () {
            var $el = $(this.el);

            $el.on('focus', function () {
                $el.select();
            });
        }
    });
    Vue.directive('toggler', {
        bind: function () {
            var $el = $(this.el),
                vm = this;

            $el.on('click', function () {
                $(vm.expression).toggle();
            });
        }
    });
    Vue.directive('autoDecimal', {
        bind: function () {
            var $el = $(this.el);

            // with BUGS
            // $el.number( true, 0, '.', ' ' );

            // with BUGS
            // $el.autoNumeric('init', {
            //     aSep: ' ',
            //     mDec: '0',
            // });
        }
    });
    Vue.directive('scrollTopOnClick', {
        bind: function () {
            var vm = this.vm,
                params = this.params,
                $el = $(this.el);

            $el.on('click', function () {
                window.scrollTo(0,0);
            });
        }
    });
    Vue.directive('crossAllOtherOnHover', {
        bind: function () {
            var $el = $(this.el);

                $el.on('mouseenter', function () {
                    $el.closest('.filter_item').find('li').addClass('crossed');
                });
                $el.on('mouseleave', function () {
                    $el.closest('.filter_item').find('li').removeClass('crossed');
                });
        }
    });
    Vue.directive('showOnBottom', {
        bind: function () {
            var $el = $(this.el),
                $window = $(window),
                $document = $(document);

            setTimeout(function() {
                isPageBottom($el);
            }, 1);

            $window.on('scroll', function () {
                isPageBottom($el);
            });

            function isPageBottom () {
                if ($window.scrollTop() + $window.height() == $document.height()) {
                    $el.show();
                } else {
                    $el.hide();
                }
            }
        }
    });
    Vue.directive('keyArrows', {
        bind: function () {
            var $el = $(this.el);
            var vm = this.vm;

            $el.on('keyup', function(e) {
                if (!vm.qv.show) {
                    return;
                }

                if (e.keyCode == 37) {
                    if (vm.qvHasPrevBtn()) {
                        vm.qvPrev();
                    }
                }

                if (e.keyCode == 39) {
                    if (vm.qvHasNextBtn()) {
                        vm.qvNext();
                    }
                }

                if (e.keyCode == 27) {
                    vm.qv.show = false;
                }
            });
        }
    });
    Vue.directive('stickIn', {
        params: ['parent'],

        bind: function () {
            var $el = $(this.el),
                params = this.params,
                parent = params.parent;

            $el.stick_in_parent({
                offset_top: 110,
                parent: parent,
            });
        }
    });
    Vue.directive('stickFiltersInCatalog', {
        update: function (filtersOn) {
            var $el = $(this.el);

            if (filtersOn) {
                setTimeout(function() {
                    $el.stick_in_parent({
                        offset_top: 100,
                        parent: '.catalog',
                    });
                }, 1);
            } else {
                $el.trigger("sticky_kit:detach");
            }
        }
    });
    Vue.directive('quickView', {
        bind: function () {
            var $el = $(this.el);
            var vm = this.vm;

            $el.on('click', '.qv-btn', function(e) {
                var $this = $(this);

                e.stopPropagation();
                e.preventDefault();

                vm.qvShow($this);
            });

            $el.on('click', '.close-btn', function() {
                vm.qv.show = false;
            });
        }
    });
    Vue.directive('ajaxDbRequestOnChangeFilter', {
        bind: function () {
            let vm = this.vm;
            $(this.el).on('change','.filter_for_request', function () {
                // vm.ajaxDbRequestFunc();
            });
        }
    });
    Vue.directive('showMoreReviews', {
        bind: function (el) {
            var $el = $(this.el);
            var vm = this.vm;
            $el.on('click', function() {
                vm.ajaxMoreReviews();
            });
        }
    });
    Vue.transition('promoted', {
        enterClass: 'slideInLeft',
        leaveClass: 'slideOutLeft',
        type: 'animation'
    });

    new Vue({
        el: 'body',
        data: vueData,
        ready: function () {
            let vm = this;

            if (window.localStorage.isRegionManuallySet !== undefined) {
                vm.isRegionManuallySet = 'true';
                // vm.isRDSaveNeeded = false;
            }

            // Показывать модальное окно
            // if (!window.localStorage.isShowMoveModal && this.zone === 'ru') {
            //     this.showMove2021AnnounceModal();
            //     window.localStorage.isShowMoveModal = true;
            // }

            vm.getUserLocation();
            vm.getReferrerInfo(document.referrer);

           // $(window).on('scroll', vm.onScroll);

            window.addEventListener('scroll', vm.onScroll, {passive:true});

            $('[data-toggle="tooltip"]').tooltip();

            //Прокрутка на странице до элемента с якорем, если он есть
            if (vm.tabSets !== undefined) {

                if (window.location.hash) {
                    if (window.location.hash == '#characteristics') {
                        vm.tabSets.isTabActive = 1;
                    }
                    if (window.location.hash == '#reviews') {
                        vm.tabSets.isTabActive = 2;
                    }

                }
            }
            // Проверяем высоту места занимаемого 'подборками' автоссылок
            if ($('.refers-item').height() > 40){
                $('.refers-show').show(); // Включаем ссылку 'Показать ещё'
            }
            // Проверяем наличие переменной reviews_submit в локальном хранилище браузера, чтобы не дать повторно оставить отзыв
            if (vm.reviews != undefined && window.localStorage.reviews_submit != undefined) {
                var reviews_item = localStorage.getItem('reviews_submit').split(',');
                vm.reviews.buttonShow = reviews_item.includes('' + vm.reviews.form.id);
            }

            // Обявления
            // Объявление о переезде офиса перенесено в логирование

            // Логи 2.0
            vm.logVisit();

            // Скрыть Яндекс Советник
            // Вариат 1 - Изменение кода, поиск по id
            try {
                var observer, callback;
                callback = function(mutations){
                    for(var i = 0; i < mutations.length; ++i) {
                        // look through all added nodes of this mutation
                        for(var j=0; j < mutations[i].addedNodes.length; ++j) {
                            let id = mutations[i].addedNodes[j].id;

                            const regex = /[a-z0-9]{8,15}/g;
                            let m;

                            while ((m = regex.exec(id)) !== null) {
                                if (m.index === regex.lastIndex) {
                                    regex.lastIndex++;
                                }
                                m.forEach((match, groupIndex) => {
                                    let elem = document.getElementById(match);
                                    if (elem && elem.parentNode && elem.nodeName === 'DIV' && elem.parentNode.nodeName === 'BODY') {
                                        // console.log('Killed Yandex.Sovetnik div with ID ' + match);
                                       elem.parentNode.removeChild(elem);
                                    } else {
                                        // console.log('Trying to kill Yandex.Sovetnik div with ID ' + match + ', but no element was found');
                                    }
                                });
                                document.getElementsByTagName('html')[0].style = '';
                                document.documentElement.removeAttribute("g_init");
                            }
                        }
                    }
                };
                observer = new MutationObserver( callback );
                let options = {
                        'childList': true,
                        'attributes':true
                    },
                    article = document.querySelector('body');

                observer.observe( article, options );
            } catch (e) {
                // console.log(e);
            }
            // Вариат 2 - Скрыть Советник, поиск по цвету
            // document.addEventListener("DOMContentLoaded", function(event) {
            //     document.body.addEventListener('DOMSubtreeModified', function () {
            //         vm.hideSovetnik();
            //         document.documentElement.removeAttribute("g_init");
            //     }, false);
            //     vm.hideSovetnik();
            //     // console.log('sovetnik');
            // });
            // Вариат 3 - удалить Style, поиск по значению z-index
            // setTimeout (function delRed() {
            //     let dds= 0;
            //     if (dds < 25) {
            //         $('style').each(function(index) {
            //             let that = $(this),
            //                 text = that.text();
            //             if (text.indexOf('2147483647') > 0) {
            //                 document.documentElement.removeAttribute('style');
            //                 that.text('#selector {opacity: 0;}');
            //                 dds=26;
            //             }
            //         });
            //         dds = dds+1;
            //         setTimeout(function() {delRed()}, 50);
            //         document.documentElement.removeAttribute("g_init");
            //     }
            // },500);
        },
        created: function () {
            window.addEventListener("resize", this.debounce(this.setVueDataIsMobileDevice));
        },
        destroyed() {
            window.removeEventListener("resize", this.setVueDataIsMobileDevice);
        },
        methods: {
            debounce:function(func){
                var timer;
                return function(event){
                    if(timer) clearTimeout(timer);
                    timer = setTimeout(func,100,event);
                };
            },
            setVueDataIsMobileDevice: function(){
                this.isMobileDevice = window.innerWidth < 992;
            },
            ajaxDbRequestFunc: function() {
                var vm = this;
                // ставим таймаут для того, чтобы Vue scope успел обновиться
                setTimeout(function() {
                    var defaultQuery = {kategoriya: vm.query.kategoriya, sort_tsena: vm.query.sort_tsena, s_tsenoi: vm.query.s_tsenoi, text: vm.query.text},
                        searchQuery = vm.filtersOn || vm.query.kategoriya instanceof Array === true ? vm.query : defaultQuery,
                        $body = $('body'),
                        $catalog = $('.catalog'),
                        loaderHtml = '<div class="la-timer la-2x"><div></div></div>';
                    if (vm.slug && vm.filterLabels.cat.length > 1 && vm.query.kategoriya.length != 1) {
                        vm.query.df.checkbox = {};
                        vm.query.df.between = {};
                    }

                    $.ajax({
                        type: 'post',
                        url: vm.ajaxUrl,
                        data: {
                            route : vm.route,
                            slug: vm.slug, //Используется для промо страниц PromoController actionIndex($slug)
                            routeParams: $.param(vm.query),
                            catDfCount: vm.catDfCount,
                            query: JSON.stringify(searchQuery),
                            url: window.location.href.toString(),
                            // isfiltersOn: vm.filtersOn,
                            // dfBetweenRanges: JSON.stringify(vm.dfBetweenRanges),
                            betweenFiltersOn: JSON.stringify(vm.betweenFiltersOn),
                        },
                        beforeSend: function () {
                            // vm.ajaxProductsHtml = loader;
                            $body.append(loaderHtml);
                            $catalog.css('opacity', '.4').css('pointer-events', 'none');
                            $catalog.find('input[type="text"]').prop('disabled', 'disabled');
                        },
                        success: function (response) {
                            var $loader = $('.la-timer');

                            $loader.remove();
                            $catalog.css('opacity', '1').css('pointer-events', 'all');
                            $catalog.find('input[type="text"]').prop('disabled', '');
                            vm.ajaxProductsHtml = response.html;
                            vm.ajaxNoProducts = response.hasOwnProperty('ajaxNoProducts') ? true : false;
                            vm.minPrice = response.minPrice;
                            vm.maxPrice = response.maxPrice;
                            vm.productsCount = response.productsCount;
                            // Выставляем отступ к нижнему описанию, в зависимости есть Пагинация или нет
                            var productCount = vm.productsCount.split(' ');
                            vm.pageDataSet.descBottomMargin = (parseInt(productCount[0].replace(/\s+/g, '')) > 30 || !parseInt(productCount[0].replace(/\s+/g, ''))) ? 30 : 0;

                            vm.autolinkSet.autolink = response.autolink;                   // Массив из ссылки категории и Автосгенерированной ссылки
                            vm.searchText = (response.searchText ? response.searchText : '');   // Строка текстового поиска по категории
                            // Настройка подборок автоссылок refer_sets
                            if (vm.referSet.refers){
                                Object.keys(vm.referSet.refers).map(function (index) {
                                    // Если ссылка 'подбоки' совпадает с текущей Автоссылкой
                                    if (vm.autolinkSet.autolink['base'] + '/' + vm.referSet.refers[index].name == vm.autolinkSet.autolink['new']) {
                                        vm.referSet.refers[index].active = true;
                                    } else {
                                        vm.referSet.refers[index].active = false;
                                    }
                                });
                            }

                            if (response.seo_array['title']) {
                                window.document.title = response.seo_array['title'];           // Изменение seo title страницы
                            }
                            // Изменение seo description страницы
                            // if (response.seo_array['desc']) {
                            //     document.querySelector('meta[name="description"]').setAttribute("content", response.seo_array['desc']);
                            //     document.querySelector('meta[property="og:description"]').setAttribute("content", response.seo_array['desc']);
                            // }
                            // Изменение seo keywords страницы
                            // if (response.seo_array['keyw']) {
                            //     document.querySelector('meta[name="keywords"]').setAttribute("content", response.seo_array['keyw']);
                            // }
                            if (response.seo_array['full_desc']) {
                                vm.pageDataSet.pageDesc = response.seo_array['full_desc'];      // Изменение описания на странице
                            }
                            if (response.seo_array['desc_bottom']) {
                                vm.pageDataSet.descBottom = response.seo_array['desc_bottom'];      // Изменение нижнего описания на странице
                            }
                            if (response.seo_array['h1_title']) {
                                vm.pageDataSet.h1_title = response.seo_array['h1_title'];      // Изменение заголовка H1 на странице
                            }

                            if (response.filters) {
                                vm.filters = response.filters;
                            }

                            if (response.df) {
                                vm.query.df = response.df;
                            }

                            if (response.filterLabels) {
                                response.filterLabels.cat = vm.filterLabels.cat;
                                response.filterLabels.vendor = vm.filterLabels.vendor;
                                vm.filterLabels = response.filterLabels;
                            }

                            vm.queryCounters = response.queryCounters;
                            // changing range slider values and min-max points
                            if (vm.query.df !== undefined && response.dfBetweenValues) {
                                for (var key in vm.query.df.between) {
                                    var min = response.dfBetweenValues[key].min,
                                        max = response.dfBetweenValues[key].max;

                                    vm.query.df.between[key].min = min;
                                    $('.slider_range.' + key).slider( 'option', 'min', min );
                                    vm.query.df.between[key].max = max;
                                    $('.slider_range.' + key).slider( 'option', 'max', max );
                                }

                                // change Between filter slider values
                                for (var key in vm.query.df.between) {
                                    var updated_Ot = response.dfBetweenValues[key].ot,
                                        updated_Do = response.dfBetweenValues[key].do;

                                    vm.query.df.between[key].ot = updated_Ot;
                                    vm.query.df.between[key].do = updated_Do;
                                    $('.slider_range.' + key).slider( 'option', 'values', [ updated_Ot, updated_Do ] );
                                }
                            }

                            // changing url query params
                            if (vm.filtersOn || vm.textOn) {

                                if (vm.autolinkSet.autolink['new']) { // Если есть сгенерированная ссылка для данных настроек фильтров
                                    window.history.replaceState('', '', vm.autolinkSet.autolink['new']);
                                } else {
                                    window.history.replaceState('', '', vm.autolinkSet.autolink['base'] + '?' + $.param(vm.query));
                                }
                            } else {
                                if (vm.isPromoPage || vm.query.kategoriya instanceof Array !== true) {
                                    let sortQuery = vm.query.sort_tsena === 'asc' ? '' : '?sort_tsena=desc';
                                    window.history.replaceState('', '', vm.autolinkSet.autolink['base'] + sortQuery);
                                } else if (vm.query.kategoriya instanceof Array === true) { // когда используется в поиске (SearchController)
                                    let sortQuery = vm.query.sort_tsena == 'asc' ? '' : '&sort_tsena=desc';
                                    window.history.replaceState('', '', window.location.pathname + '?term=' + vm.query.term.replace(/\s/g, '+') + sortQuery);
                                }
                            }
                        }
                    });
                }, 1);
            },

            isMobile: function() {
                return $(window).width() < 768;
            },
            onScroll: function() {
                var scrollTop = $(window).scrollTop();

                if (scrollTop > 340) {
                    $('.filters-mobile.stick').removeClass('hide');
                } else {
                    $('.filters-mobile.stick').addClass('hide');
                }
            },
            toggleMenu: function(e) {
                e.preventDefault();

                var vm = this;

                var $mmenu = $('#mmenu');
                var API = $mmenu.data('mmenu');

                if (!$mmenu.hasClass('mm-opened')) {
                    API.open();
                } else {
                    API.close();
                }
            },
            // slideoutMenu: function () {
            //     slideout.toggle();
            // },
            getUserLocation: function () {
                var vm = this;

                if (window.localStorage.uIP === undefined) {
                    delete window.localStorage.region;
                    delete window.localStorage.regionAuto;
                    window.localStorage.uIP = vm.uIP;
                }

                if (window.localStorage.regionAuto === undefined) {
                    $.ajax({
                        type: 'get',
                        url: '/ajax-get-user-location',
                        data: {},
                        success: function (response) {
                            if (response.status == 'success') {
                                var city = response.city;
                            } else {
                                city = vm.defaultRegion;
                            }

                            vm.region     = window.localStorage.region     = city;
                            vm.regionAuto = window.localStorage.regionAuto = city;
                        },
                        error: function (response) {
                            var city = vm.defaultRegion;

                            vm.region     = window.localStorage.region     = city;
                            vm.regionAuto = window.localStorage.regionAuto = city;
                        },
                        complete: function (response) {
                            vm.setIsFromAnotherRegion();
                            if (vm.deliveryData !== undefined) {
                                vm.deliveryData = vm.getDeliveryData();
                            }

                            // vm.saveRegionData(response.responseJSON);
                        }
                    });

                } else {
                    vm.region     = window.localStorage.region ? window.localStorage.region : defaultRegion;
                    vm.regionAuto = window.localStorage.regionAuto;

                    vm.setIsFromAnotherRegion();
                    if (vm.deliveryData !== undefined) {
                        vm.deliveryData = vm.getDeliveryData();
                    }
                }
            },
            getReferrerInfo: function (referrer) {
                vm = this;

                // if (window.localStorage.refHost === undefined || !this.isDirectVisit) {
                    $.ajax({
                        type: 'get',
                        url: '/ajax-get-referrer-info',
                        data: {
                            referrer: referrer,
                        },
                        success: function (response) {
                            vm.refHost    = response.host;
                            vm.refRequest = response.request;
                            vm.utm_term = response.utm_term;
                            vm.utm_campaign = response.utm_campaign;
                            // vm.refHost    = window.localStorage.refHost    = response.host;
                            // vm.refRequest = window.localStorage.refRequest = response.request;
                        }
                    });
            //     } else {
            //         vm.refHost    = window.localStorage.refHost;
            //         vm.refRequest = window.localStorage.refRequest;
            //     }
            // },
            // isDirectVisit: function () {
            //     if (document.referrer) {
            //         return document.referrer.indexOf(location.host) != -1 ? true : false;
            //     }

            //     return true;

            },
            removeAllFilters: function () {
                this.query.nalichie = false;
                this.query.text = '';
                this.searchText = '';
                this.query.s_tsenoi = true;
                this.query.tsena_ot = [];
                this.query.tsena_do = [];
                // this.query.typeIds = [];
                if (this.query.kategoriya instanceof Array === true) {
                    this.query.kategoriya = [];
                }
                this.query.brend = [];

                // resetting all Dynamic filters
                if (this.query.df !== undefined) {
                    for (var key in this.query.df.checkbox) {
                        this.query.df.checkbox[key] = [];
                    }
                    for (var key in this.query.df.between) {
                        Vue.set(this.query.df.between[key], 'ot', this.query.df.between[key].min); //reset filter to min value
                        Vue.set(this.query.df.between[key], 'do', this.query.df.between[key].max); //reset filter to max value
                    }
                }

                this.ajaxDbRequestFunc();

            },
            setRegion: function (region) {
                var vm = this;

                vm.modalOverlay = false;
                vm.otherRegion = '';
                vm.region = window.localStorage.region = region;
                vm.otherRegionInput = false;
                vm.regionModal = false;
                vm.isRegionManuallySet = window.localStorage.isRegionManuallySet = 'true';
                vm.setIsFromAnotherRegion();
                vm.deliveryData = vm.getDeliveryData();
                if (vm.qv.product.is_available) {
                    Vue.set(vm.qv.product, 'deliveryData', vm.getDeliveryData(vm.qv.product.is_available, vm.qv.product.priceFinal, vm.qv.product.is_change_order_7_days));
                }

                // if (vm.isRDSaveNeeded) {
                //     vm.saveRegionData();
                // }
            },
            // saveRegionData: function (response) { // DEPRECATED
            //     var vm = this,
            //         regionDetails = response !== undefined ? response.regionName + ' (' + response.country + ')' : '',
            //         regionIP = response !== undefined ? response.query : '';
            //
            //     $.ajax({
            //         type: 'post',
            //         url: '/ajax-save-region-data',
            //         data: {
            //             id: vm.rdID,
            //             autoRegionName: vm.regionAuto,
            //             customRegionName: vm.region,
            //             regionDetails: regionDetails,
            //             regionIP: regionIP,
            //             _csrf: yii.getCsrfToken(),
            //         },
            //         success: function (response) {
            //             if (response.id) {
            //                 vm.rdID = response.id;
            //             }
            //         },
            //         error: function (response) {},
            //     });
            // },
            setRegionFromDropdown: function () {
                this.otherRegion = '';
                window.localStorage.region = this.region;
                this.otherRegionInput = false;
            },
            activateOtherRegion: function () {
                this.otherRegionInput = !this.otherRegionInput;
            },
            setOtherRegion: function () {
                this.modalOverlay = false;
                window.localStorage.region = this.otherRegion;
                this.regionModal = false;
            },
            searchActivate: function () {
                $('#search-term').focus();
            },
            clearSearch: function () {
                var vm = this;

                vm.searchTerm = '';

                vm.searchExampleInterval = setInterval(function() {
                    var rand = Math.floor(Math.random() * vm.searchExamples.length);

                    vm.searchExample = vm.searchExamples[rand];
                }, 10000);
            },
            getProductImage: function (imagePath, size, zone) {
                return process.env.MIX_APP_URL + zone + imagePath.replace('@size', '@' + size);
            },
            decreaseQuantity: function (key) {
                this.cartProducts[key].quantity--;
            },
            increaseQuantity: function (key) {
                this.cartProducts[key].quantity++;
            },
            checkNumeric: function (event) {
                var charCode = (event.which) ? event.which : event.keyCode;

                return !(charCode > 31 && (charCode < 48 || charCode > 57));
            },
            splitNumber: function (number, by, delimiter = '-') {
                let re = new RegExp('\\B(?=(\\d{'+by+'})+(?!\\d))', 'g');
                return number.toString().replace(re, delimiter);
            },

            showRegionModal: function () {
                var vm = this;

                vm.regionModal = true;
                vm.modalOverlay = true;
            },
            showEmailModal: function () {
                var vm = this;

                vm.emailModal = true;
                vm.modalOverlay = true;

                // Цель Yandex.Metrika
                if (typeof ym === 'function') {
                    ym(vm.yaMetrikaId, 'reachGoal', 'HEADER_EMAILS');
                }

                // Цель Google Analytics
                if (typeof gtag === 'function') {
                    gtag('event', 'click_email_nav', {
                        'event_category': 'info',
                    });
                }
            },
            showPhoneModal: function () {
                var vm = this;

                vm.phoneModal = true;
                vm.modalOverlay = true;

                // Цель Yandex.Metrika
                if (typeof ym === 'function') {
                    ym(vm.yaMetrikaId, 'reachGoal', 'HEADER_PHONES');
                }

                // Цель Google Analytics
                if (typeof gtag === 'function') {
                    gtag('event', 'click_phone_nav', {
                        'event_category': 'info',
                    });
                }
            },

            showProductInfo_supportModal: function () {
                var vm = this;

                vm.productInfo_supportModal = true;
                vm.modalOverlay = true;
            },
            showProductInfo_deliveryModal: function () {
                var vm = this;

                vm.productInfo_deliveryModal = true;
                vm.modalOverlay = true;
            },
            showProductInfo_deliverySelfModal: function () {
                var vm = this;

                vm.productInfo_deliverySelfModal = true;
                vm.modalOverlay = true;
            },

            showRequestModal: function (e) {
                var vm = this;
                vm.orderModal = true;
                vm.modalOverlay = true;

                // Цель Yandex.Metrika
                if (typeof ym === 'function') {
                    ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_CART_BTN');
                }

                // Цель Google Analytics
                if (typeof gtag === 'function') {
                    gtag('event', 'click_cart_btn', {
                        'event_category': 'buttons',
                    });
                }
            },
            qvShowRequestModal: function (e) {
                var vm = this;
                var data = e.target.dataset;

                vm.modalOverlay = true;
                $('body').scrollTo(0);

                if ('bs' in vm) {
                    vm.bs.show = false;
                }

                vm.orderType = 'normalOrder';

                vm.qv.persists = false;

                if (data.product_id) {
                    // vm.qv.type = data.order_type;
                    vm.qv.product.id = data.product_id;
                    vm.qv.product.parentAlias = data.product_parent_alias_name;
                    vm.qv.product.catAlias = data.product_cat_alias_name;
                    vm.qv.product.product = data.product_name;
                    vm.qv.product.alias = data.product_alias_name;
                    vm.qv.product.parent = data.product_parent_name;
                    vm.qv.product.parentId = data.product_parent_id;
                    vm.qv.product.vendor = data.product_vendor_name;
                    vm.qv.product.part_number = data.product_part_number;
                    vm.qv.product.status = data.product_status;
                    vm.qv.product.gpl = data.product_gpl;
                    vm.qv.product.price_client = data.product_price_client;
                    vm.qv.product.price_alfakz = data.product_price_alfakz;
                    vm.qv.product.exchangeRateGeneral = data.exchange_rate_general;
                    vm.qv.product.is_promo = data.product_is_promo;
                    vm.qv.product.support_price = data.product_support_price;
                    vm.qv.product.isSupportNeeded = vm.isSupportNeeded;
                } else {
                    vm.qv.persists = true;
                }

                vm.qv.orderModal = true;
                vm.qv.show = false;

                // Цель Yandex.Metrika
                if (typeof ym === 'function') {
                    ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_ORDER_BTN');
                }

                // Цель Google Analytics
                if (typeof gtag === 'function') {
                    gtag('event', 'click_order_btn', {
                        'event_category': 'buttons',
                    });
                }
            },
            showDiscountModal: function (e) {
                var vm = this;
                var data = e.target.dataset;

                vm.modalOverlay = true;
                $('body').scrollTo(0);

                // if ('bs' in vm) {
                //     vm.bs.show = false;
                // }

                vm.orderType = 'discountOrder';

                // vm.qv.persists = false;

                if (data.product_id) {
                    // vm.qv.type = data.order_type;
                    vm.qv.product.id = data.product_id;
                    vm.qv.product.parentAlias = data.product_parent_alias_name;
                    vm.qv.product.catAlias = data.product_cat_alias_name;
                    vm.qv.product.product = data.product_name;
                    vm.qv.product.alias = data.product_alias_name;
                    vm.qv.product.parent = data.product_parent_name;
                    vm.qv.product.parentId = data.product_parent_id;
                    vm.qv.product.vendor = data.product_vendor_name;
                    vm.qv.product.part_number = data.product_part_number;
                    vm.qv.product.status = data.product_status;
                    vm.qv.product.gpl = data.product_gpl;
                    vm.qv.product.price_client = data.product_price_client;
                    vm.qv.product.price_alfakz = data.product_price_alfakz;
                    vm.qv.product.exchangeRateGeneral = data.exchange_rate_general;
                    vm.qv.product.is_promo = data.product_is_promo;
                    vm.qv.product.support_price = data.product_support_price;
                    vm.qv.product.isSupportNeeded = vm.isSupportNeeded;
                }
                // else {
                //     vm.qv.persists = true;
                // }

                vm.discountModal = true;
                vm.qv.show = false;

                // Цель Yandex.Metrika
                if (typeof ym === 'function') {
                    ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_GET_DISCOUNT_BTN');
                }

                // Цель Google Analytics
                if (typeof gtag === 'function') {
                    gtag('event', 'click_discount_btn', {
                        'event_category': 'buttons',
                    });
                }
            },
            showConsultModal: function (e) {
                var vm = this;
                var data = e.target.dataset;

                vm.modalOverlay = true;
                $('body').scrollTo(0);

                vm.orderType = 'consultOrder';

                // if ('bs' in vm) {
                //     vm.bs.show = false;
                // }

                // vm.qv.persists = false;

                if (data.product_id) {
                    // vm.qv.type = data.order_type;
                    vm.qv.product.id = data.product_id;
                    vm.qv.product.parentAlias = data.product_parent_alias_name;
                    vm.qv.product.catAlias = data.product_cat_alias_name;
                    vm.qv.product.product = data.product_name;
                    vm.qv.product.alias = data.product_alias_name;
                    vm.qv.product.parent = data.product_parent_name;
                    vm.qv.product.parentId = data.product_parent_id;
                    vm.qv.product.vendor = data.product_vendor_name;
                    vm.qv.product.part_number = data.product_part_number;
                    vm.qv.product.status = data.product_status;
                    vm.qv.product.gpl = data.product_gpl;
                    vm.qv.product.price_client = data.product_price_client;
                    vm.qv.product.price_alfakz = data.product_price_alfakz;
                    vm.qv.product.exchangeRateGeneral = data.exchange_rate_general;
                    vm.qv.product.is_promo = data.product_is_promo;
                    vm.qv.product.support_price = data.product_support_price;
                    vm.qv.product.isSupportNeeded = vm.isSupportNeeded;
                }
                // else {
                //     vm.qv.persists = true;
                // }

                vm.consultModal = true;
                vm.qv.show = false;

                // Цель Yandex.Metrika
                if (typeof ym === 'function') {
                    ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_CONSULTATION_BTN');
                }

                // Цель Google Analytics
                if (typeof gtag === 'function') {
                    gtag('event', 'click_consult_btn', {
                        'event_category': 'buttons',
                    });
                }
            },

            showQuarantineAnnounceModal: function () {
                this.quarantineAnnounceModal = true;
                this.modalOverlay = true;
            },
            showMove2020AnnounceModal: function () {
                this.move2020AnnounceModal = true;
                this.modalOverlay = true;
            },
            showMove2021AnnounceModal: function () {
                this.move2021AnnounceModal = true;
                this.modalOverlay = true;
            },

            qvShow: function($btn) {
                let vm = this;
                let productId = $btn.data('id');

                if (productId === undefined) {
                    return;
                }

                vm.qv.$btn = $btn;
                vm.qv.show = true;

                vm.qv.hasPrev = vm.qvHasPrevBtn();
                vm.qv.hasNext = vm.qvHasNextBtn();

                vm.qv.loading = true;
                if (productId in cache.qv) {
                    vm.qvSetParams(cache.qv[productId]);
                } else {

                    if (('jqXHR' in vm.qv) && vm.qv.jqXHR) {
                        vm.qv.jqXHR.abort();
                    }

                    vm.qv.jqXHR = $.ajax({
                        url: '/products/ajax-product/' + productId,
                        type: 'post',
                        dataType: 'json',
                        success: function(response) {
                            if (('status' in response) && response.status === 'ok') {
                                cache.qv[productId] = response.product;

                                vm.qvSetParams(response.product);
                                vm.qv.loading = false;
                            }
                        }
                    });
                }
            },
            qvSetParams: function(params) {
                let vm = this,
                    product = params,
                    $owlCarousel = $('#qvProductPhotoSlider');

                vm.qv.successAdded = false;
                vm.qv.product = params;
                Vue.set(vm.qv.product, 'deliveryData', vm.getDeliveryData(vm.qv.product.is_available, vm.qv.product.priceFinal, vm.qv.product.is_change_order_7_days));

                // Пересоздаем слайдер
                vm.qvLoadSlider($owlCarousel);

                // console.log('vm.qv.product.id = ' + vm.qv.product.id);
                // console.log('vm.visitData.visitUid = ' + vm.visitData.visitUid);
                if (vm.qv.product.id && vm.visitData.visitUid) {
                    vm.defineQuickCode(vm.qv.product.id, vm.visitData.visitUid - 100000, true);
                }


                vm.qv.loading = false;
            },

            qvLoadSlider: function ($owlCarousel) {
                let vm = this,
                    html = '';

                // Новое наполнение слайдера
                vm.qv.product.images[500].forEach(function (image, index) {
                    html += '<div class="photo-item product-photo">';
                    if (vm.qv.product.images[1000][index].path.length > 0) {
                        html += '<div class="zoom-wrapper" data-src="' + vm.qv.product.images[1000][index].path + '">';
                        html += '<img class="photo"' +
                            ' itemprop="image"' +
                            ' src="' + image.path + '"' +
                            ' data-thumbnail="' + vm.qv.product.images[150][index].path + '"' +
                            ' alt="' + vm.qv.product.name + '">';
                        html += '</div>';
                    } else {
                        html += '<img class="photo"' +
                            ' itemprop="image"' +
                            ' src="' + image.path + '"' +
                            ' data-thumbnail="' + vm.qv.product.images[150][index].path + '"' +
                            ' alt="' + vm.qv.product.name + '">';
                    }
                    html += '</div>';
                });

                // Уничтожаем старый слайдер owl carousel.
                // Но только если он существует (игнорируем уничтожение на первый раз).
                if (!vm.isEmpty($owlCarousel)) {
                    $owlCarousel.data('owlCarousel').destroy();
                }
                // Заменяем контент слайдера на новый
                $owlCarousel.html(html);
                // Создаем новый слайдер owl carousel
                $owlCarousel.owlCarousel({
                    autoPlay: 5000,
                    items : 1,
                    itemsCustom : [1200, 1],
                    mouseDrag: true,
                    pagination: true,
                    stopOnHover: true,
                    afterUpdate: function (elem) {
                        vm.setSliderPagination($(elem));
                    },
                    afterInit: function (elem) {
                        vm.setSliderPagination($(elem));
                    },
                });

                // Меняем пагинацию на миниатюры
                // vm.setSliderPagination($owlCarousel);
                // Добавляем jquery zoom, кроме мобильных
                if (!vm.isMobile()) {
                    vm.qvZoom($owlCarousel);
                }
            },
            setSliderPagination: function ($owlCarousel) {
                // Меняем пагинацию слайдера на миниатюры
                let $pagination = $owlCarousel.find('.owl-pagination');
                $pagination.addClass('product-photos');

                // Проставляем порядковые номера слайдам
                let slidesCount = 0;
                $owlCarousel.find('.owl-item').not('.cloned').each(function () {
                    slidesCount++;
                    $(this).attr('data-number', slidesCount);
                });

                // Меняем "точки" пагинации.
                // Проставляем src пагинации из data-thumbnail соответствующего слайда.
                let dotsCount = 0;
                $pagination.find('.owl-page').each(function () {
                    dotsCount++;
                    let $dot = $owlCarousel.find('[data-number="' + dotsCount + '"]');
                    let url = $dot.find('[itemprop="image"]').data('thumbnail');
                    $(this).addClass('photo-dot');
                    $(this).html('<img src="' + url + '" style="width:100%;height:auto;" width="150" height="150" loading="lazy"/>');
                });
            },
            qvZoom: function($owlCarousel) {
                // Инициализируем jquery zoom на слайдах если у них есть обёртка zoom-wrapper
                $owlCarousel.find('.zoom-wrapper').each(function () {
                    let $wrapper = $(this);
                    let src = $wrapper.data('src');
                    $(this).zoom({
                        url: src,
                        deadZone: 200,
                        on: 'click',
                        onZoomIn: function () {
                            $wrapper.addClass('zoom-in');
                        },
                        onZoomOut: function () {
                            $wrapper.removeClass('zoom-in');
                        }
                    });
                });
            },

            qvGetPrevBtn: function() {
                var vm = this;

                var $prev = vm.qv.$btn.parents('.product_item');
                var $parent = $prev.parent();
                var $btn;

                if ($parent.hasClass('owl-item')) {
                    $prev = $parent;
                }

                $prev = $prev.prev();

                if ($prev.length) {
                    $btn = $prev.find('.qv-btn');
                }

                if ($btn !== undefined && $btn.length) {
                    return $btn;
                }

                return undefined;
            },
            qvGetNextBtn: function() {
                var vm = this;

                var $next = vm.qv.$btn.parents('.product_item');
                var $parent = $next.parent();
                var $btn;

                if ($parent.hasClass('owl-item')) {
                    $next = $parent;
                }

                $next = $next.next();

                if ($next.length) {
                    $btn = $next.find('.qv-btn');
                }

                if ($btn !== undefined && $btn.length) {
                    return $btn;
                }

                return undefined;
            },

            qvHasPrevBtn: function() {
                var vm = this;
                var $btn = vm.qvGetPrevBtn();

                return $btn !== undefined;
            },
            qvHasNextBtn: function() {
                var vm = this;
                var $btn = vm.qvGetNextBtn();

                return $btn !== undefined;
            },

            qvPrev: function() {
                var vm = this;
                var $btn = vm.qvGetPrevBtn();

                if ($btn !== undefined) {
                    vm.qvShow($btn);
                }
            },
            qvNext: function() {
                var vm = this;
                var $btn = vm.qvGetNextBtn();

                if ($btn !== undefined) {
                    vm.qvShow($btn);
                }
            },

            bsShow: function() {
                var vm = this;
                vm.bs.show = true;

                if ('$bxSlider' in vm.bs) {
                    setTimeout(function() {
                        vm.bs.$bxSlider.reloadSlider();
                    }, 50);
                }
            },
            bsHide: function() {
                var vm = this;
                vm.bs.show = false;
            },

            showFilterSort: function() {
                $('.m-filter-sort').data('mmenu').open();
            },
            hideFilterSort: function() {
                $('.m-filter-sort').data('mmenu').close();
            },

            showFilterFilters: function() {
                $('.m-filter-filters').data('mmenu').open();
            },
            hideFilterFilters: function() {
                $('.m-filter-filters').data('mmenu').close();
            },

            setIsFromAnotherRegion: function () {
                if (this.isRegionManuallySet == 'true') {
                    this.isFromAnotherRegion = this.region != this.defaultRegion;
                    return;
                } else {
                    for (var key in this.defaultRegionAliases) {
                        if (this.regionAuto === this.defaultRegionAliases[key]) {
                            this.isFromAnotherRegion = false;
                            return;
                        }
                    }
                }

                this.isFromAnotherRegion = true;
            },
            getDeliveryData: function (productIsAvailable, productPrice, productIsChangeOrder7Days) {
                let vm = this,
                    _deliveryData = {},
                    today = vm.datesData.today,
                    tomorrow = vm.datesData.tomorrow,
                    dayAfterTomorrow = vm.datesData.dayAfterTomorrow,
                    nextWorkday = vm.datesData.nextWorkday,
                    nextWorkdayString = vm.datesData.nextWorkdayString,
                    sendTime = nextWorkday === tomorrow ? 'завтра' : nextWorkday === dayAfterTomorrow ? 'послезавтра' : nextWorkdayString,
                    isFromAnotherRegion = vm.isFromAnotherRegion;

                productPrice = productPrice ? productPrice : vm.productPrice;
                productIsAvailable = productIsAvailable ? productIsAvailable : vm.productIsAvailable;
                productIsChangeOrder7Days = parseInt(productIsChangeOrder7Days ? productIsChangeOrder7Days : vm.productIsChangeOrder7Days);

                switch (parseInt(productIsAvailable)) {
                    case vm.availConst['order']:
                        _deliveryData['isDelivery'] = true;
                        _deliveryData['deliveryText'] = isFromAnotherRegion ? 'Доставка ТК' : 'Доставка';
                        _deliveryData['deliverySelfText'] = 'Самовывоз';
                        _deliveryData['start'] = isFromAnotherRegion ? 'заказ до Алматы 6-8 недель' : '';
                        _deliveryData['end'] = isFromAnotherRegion ? 'через 2-8 дней' : 'через 6-8 недель';
                        _deliveryData['self'] = 'через 6-8 недель';
                        _deliveryData['total'] = isFromAnotherRegion ? 'через 7-9 недель' : 'через 6-8 недель';
                        break;
                    case vm.availConst['order7Days']:
                        _deliveryData['isDelivery'] = true;
                        _deliveryData['deliveryText'] = isFromAnotherRegion ? 'Доставка ТК' : 'Доставка';
                        _deliveryData['deliverySelfText'] = 'Самовывоз';
                        _deliveryData['start'] = productIsChangeOrder7Days ? 'уточняйте у менеджера' : isFromAnotherRegion ? 'заказ до Алматы 7-12 дней' : '';
                        _deliveryData['end'] = productIsChangeOrder7Days ? 'уточняйте у менеджера' : isFromAnotherRegion ? 'через 2-8 дней' : 'через 7-12 дней';
                        _deliveryData['self'] = productIsChangeOrder7Days ? 'уточняйте у менеджера' : 'через 7-12 дней';
                        _deliveryData['total'] = productIsChangeOrder7Days ? 'уточняйте у менеджера' : isFromAnotherRegion ? 'через 9-20 дней' : 'через 7-12 дней';
                        break;
                    case vm.availConst['available']:
                        _deliveryData['isDelivery'] = true;
                        _deliveryData['deliveryText'] = isFromAnotherRegion ? 'Доставка ТК' : 'Доставка курьером';
                        _deliveryData['deliverySelfText'] = 'Самовывоз со склада';
                        _deliveryData['start'] = isFromAnotherRegion ? 'через транспортную компанию ' + sendTime : '';
                        _deliveryData['end'] = isFromAnotherRegion ? 'через 2-8 дней' : sendTime;
                        _deliveryData['self'] = sendTime;
                        _deliveryData['total'] = isFromAnotherRegion ? 'через 2-8 дней' : sendTime;
                        break;
                    default:
                        _deliveryData['isDelivery'] = false;
                        _deliveryData['deliveryText'] = 'Доставка';
                        _deliveryData['deliverySelfText'] = 'Самовывоз';
                        // _deliveryData['start'] = '';
                        // _deliveryData['end'] = 'недоступно';
                        _deliveryData['self'] = 'недоступен';
                        _deliveryData['total'] = 'недоступна';
                }

                if (_deliveryData.isDelivery) {
                    if (!isFromAnotherRegion) {
                        let isFreeDelivery = productPrice > vm.deliveryPriceData['freeFrom'];

                        if (isFreeDelivery) {
                            _deliveryData['price'] = 'бесплатно';
                            _deliveryData['priceCond'] = 'free';
                        } else {
                            _deliveryData['price'] = vm.deliveryPriceData['price'] + ' ' + vm.currency;
                            _deliveryData['priceCond'] = 'exists';
                        }
                    } else {
                        _deliveryData['price'] = 'цену уточняйте';
                        _deliveryData['priceCond'] = 'refine';
                    }
                } else {
                    _deliveryData['price'] = 'недоступна';
                    _deliveryData['priceCond'] = 'unavailable';
                }

                _deliveryData['isReady'] = true;

                return _deliveryData;
            },

            removeTextFilter: function () {
                vm.query['text'] = '';

                vm.ajaxDbRequestFunc();
            },
            changeTabAnchor: function (anchor) {
                window.location.href= this.tabSets.selfUrl + anchor;
                // var title = '';
                // var breadcrumbs = $('.breadcrumb li');                  // Получаем массив "хлебных крошек"
                // var breads = [];
                // for (var i = 0; i < 4; i++) {
                //     breads[i] = breadcrumbs[i].outerHTML;               // Новый массив "хлебных крошек" до 4 элемента (категория) включительно
                // }
                //
                // switch (anchor) {
                //     case '/harakteristiki' :
                //         this.tabSets.isTabActive = 1;                               // таб "Характеристики" включен
                //         this.tabSets.tabName = 'Характеристики';                    // Название таба, для подстановки в заголовок и "хлебные крошки"
                //         title = this.tabSets.tabName + ' ' + this.productFullName;  // создаем строку для заголовка H1
                //         // Вставляем последние элементы в "хлебные крошки"
                //         breads.push('<li class="hidden-xs hidden-sm"><a href="' + this.tabSets.selfUrl + '">' + this.productShortName + '</a></li>');
                //         breads.push('<li class="active hidden-xs hidden-sm"><span>' + this.tabSets.tabName + '</span></li>');
                //         window.document.title = this.tabSets.seoTitle.characteristics;// изменяем title страницы
                //         break;
                //     case '/otzyvy' :
                //         this.tabSets.isTabActive = 2;                               // таб "Характеристики" включен
                //         this.tabSets.tabName = 'Отзывы';                            // Название таба, для подстановки в заголовок и "хлебные крошки"
                //         title = this.tabSets.tabName + ' на ' + this.productFullName;// создаем строку для заголовка H1
                //         // Вставляем последние элементы в "хлебные крошки"
                //         breads.push('<li class="hidden-xs hidden-sm"><a href="' + this.tabSets.selfUrl + '">' + this.productShortName + '</a></li>');
                //         breads.push('<li class="active hidden-xs hidden-sm"><span>' + this.tabSets.tabName + '</span></li>');
                //         window.document.title = this.tabSets.seoTitle.reviews;      // изменяем title страницы
                //         break;
                //     case '' :
                //         this.tabSets.isTabActive = 0;                               // таб "Описание" включен
                //         this.tabSets.tabName = '';                                  // Название таба, для подстановки в заголовок и "хлебные крошки"
                //         title = this.productFullName;                               // создаем строку для заголовка H1
                //         // Вставляем последний элемент в "хлебные крошки"
                //         breads.push('<li class="active hidden-xs hidden-sm"><span>' + this.productShortName + '</span></li>');
                //         window.document.title = this.tabSets.seoTitle.main;         // изменяем title страницы
                //         break;
                // }
                //
                // this.tabSets.title_h1 = title;                                      // Изменяем заголовок H1 на странице
                // window.history.replaceState('', '', this.tabSets.selfUrl + anchor); // Изменяем Url на странице
                // $('.breadcrumb').html(breads);                                      // Заменяем на странице "хлебные крошки" на новые
            },
            setUrlOnLoad: function () {
                var url = window.location.toString();
                var arr_url = url.split('#');
                if (arr_url[1]) {                                       // Проверяем на наличие якоря в ссылке
                    this.tabSets.anchor = arr_url[1];                   // запоминаем якорь, чтобы перейти по нему
                    if (arr_url[1] == 'product-tech-params') {          // если якорь на таб "Характеристики"
                        this.changeTabAnchor('/harakteristiki');        // запускаем функцию изменения данных при активном табе "Характеристики"
                    } else {
                        if (arr_url[1] == 'product-reviews') {          // если якорь на таб "Отзывы"
                            this.changeTabAnchor('/otzyvy');            // запускаем функцию изменения данных при активном табе "Отзывы"
                        } else {
                            this.changeTabAnchor('');                   // запускаем функцию изменения данных с данными по умолчанию
                        }
                    }
                }
            },
            changeReferSet: function (referLink, idCat) {
                var vm = this;
                setTimeout(function() {
                    $.ajax({
                        type: 'post',
                        url: vm.referSet.ajaxUrlRefer,
                        data: {
                            url: referLink,                             // Строка с ссылкой 'подборки'
                            id: idCat                                   // id категории
                        },
                        success: function (response) {
                            var filters = {};
                            if (response){
                                filters = JSON.parse(response);         // преобразуем строку с настройками в объект
                            }
                            vm.removeAllFilters();                      // Сбрасываем все фильтры к начальным значениям
                            if (filters.brend != undefined) {
                                vm.query.brend = filters.brend;         // Устанавливаем значение фильтра Бренд
                            }
                            // Проверяем наличие настроек для фильтров типа Between
                            if (filters.df != undefined && filters.df.between != undefined) {
                                var between = filters.df.between;
                                // Устанавливаем значение фильтров типа Between
                                Object.keys(between).map(function (index) {
                                    vm.query.df.between[index] = between[index];
                                });
                            }
                            // Проверяем наличие настроек для фильтров типа Checkbox
                            if (filters.df != undefined && filters.df.checkbox != undefined) {
                                var checkbox = filters.df.checkbox;
                                // Устанавливаем значение фильтров типа Checkbox
                                Object.keys(checkbox).map(function (index) {
                                    vm.query.df.checkbox[index] = checkbox[index];
                                });
                            }
                            vm.ajaxDbRequestFunc();                     // запускаем изменение данных на странице
                        }
                    });
                }, 1);
            },

            // Проверка пуст ли элемент
            isEmpty: function (el) {
                return !$.trim(el.html())
            },
            // Отправка формы таба "Отзывы"
            reviewsFormSubmit: function () {
                var vm = this;
                var post = $('#review-form').serialize();                                    // Получаем данные формы
                var rating = $('#review-form .star-rating__star.is-selected');               // Получаем заполненые данные Оценки

                if (vm.reviews.form.name && (vm.reviews.form.comment) && rating.length) {   // Проверка обязательных полей
                    setTimeout(function() {
                        $.ajax({
                            type: 'post',
                            url: vm.reviews.ajaxUrl,                                        // url для обработки формы
                            data: post,
                            success: function (response) {
                                vm.reviews.formShow = !vm.reviews.formShow;                 // Скрываем форму
                                vm.modalOverlay = true;
                                vm.reviews.responseModal = true;                            // Показываем модальное окно
                                if (window.localStorage.reviews_submit != undefined) {      // Проверяем наличие reviews_submit в localStorage
                                    var reviews_item = localStorage.getItem('reviews_submit');
                                    // Добавляем id товара в localStorage
                                    localStorage.setItem('reviews_submit', reviews_item + ',' + vm.reviews.form.id);
                                } else {
                                    localStorage.setItem('reviews_submit', vm.reviews.form.id);// Добавляем id товара в localStorage
                                }
                            }
                        });
                    }, 1);
                } else {
                    if (!vm.reviews.form.name) {                                            // Если пусто поле Имя
                        $('#review-form label[for=author_name]').css('color','#d9534f');
                        $('#review-form input[name=author_name]').css('border-color','#d9534f');
                        $('#review-form input[name=author_name]').focus();
                    }
                    if (!vm.reviews.form.comment) {                                         // Если пусто поле Комментарий
                        $('#review-form label[for=comment]').css('color','#d9534f');
                        $('#review-form textarea[name=comment]').css('border-color','#d9534f');
                        $('#review-form textarea[name=comment]').focus();
                    }
                    if (!rating.length) {                                                   // Если не выставлена Оценка
                        $('#review-form label[for=rating]').css('color','#d9534f');
                        $('#review-form input[name=rating][value=1]').focus();
                    }
                }
            },
            // Метод проверки заполенения обязательных полей
            reviewButtonDisabled: function () {
                var vm = this;
                var rating = $('#review-form .star-rating__star.is-selected');
                // Проверка заполенения обязательных полей
                vm.reviews.ButtonDisabled = !(vm.reviews.form.name && (vm.reviews.form.comment) && rating.length);
                if (vm.reviews.form.name) {     // Если поле Имя не пустое возвращаем нормальные цвета
                    $('#review-form label[for=author_name]').css('color','');
                    $('#review-form input[name=author_name]').css('border-color','');
                }
                if (vm.reviews.form.comment) {  // Если поле Комментарий не пустое возвращаем нормальные цвета
                    $('#review-form label[for=comment]').css('color','');
                    $('#review-form textarea[name=comment]').css('border-color','');
                }
                if (rating.length) {            // Если Оценка заполнено возвращаем нормальные цвета
                    $('#review-form label[for=rating]').css('color','');
                }
            },
            // Метод для показа или скрыти формы "Оставить отзыв" и запроса Google captcha
            reviewFormShow: function () {
                var vm = this;
                var siteKey = vm.reviews.form.siteKey;
                vm.reviews.formShow = !vm.reviews.formShow;              // Показать форму "Отзывы"
                grecaptcha.ready(function () {
                    grecaptcha.execute(siteKey, {action: 'add_comment'}) // Запускает проверку captcha
                        .then(function (token) {
                            $(function () {
                                $.ajax({
                                    type: 'post',
                                    url: '/products/verify-captcha',     // url проверки captcha
                                    data: {
                                        action: 'add_comment',
                                        token: token,
                                    },
                                    success: function (response) {
                                        response = JSON.parse(response);
                                        vm.reviews.form.success = response.success;
                                        if (response.success) {
                                            vm.reviews.form.score = response.score;
                                        }
                                    }
                                });
                            });
                        });
                });
            },
            // Метод закрытия модального окна после отправки Отзыва
            responseModalClose: function () {
                var vm = this;
                vm.modalOverlay = false;
                vm.reviews.responseModal = false;
                window.location.reload();
            },
            // Метод для показа отзывов по странично
            ajaxMoreReviews: function () {
                var vm = this,
                    // Строка данных для запроса
                    post = 'id=' + vm.reviews.form.id + '&count=' + vm.reviews.reviewCountPerPage + '&page=' + vm.reviews.pageNumber,
                    $body = $('body'),                                              // контейнер для loader-а
                    $mainBox = $('.product-full-desc'),                             // контейнер, который будет неактивным
                    loaderHtml = '<div class="la-timer la-2x"><div></div></div>';   // Структура loader-а
                $.ajax({
                    type: 'post',
                    url: '/products/ajax-more-reviews',
                    data: post,
                    beforeSend: function () {
                        // Показываем loader
                        $body.append(loaderHtml);
                        $mainBox.css('opacity', '.4').css('pointer-events', 'none');
                    },
                    success: function (response) {
                        var $loader = $('.la-timer');
                        // Скрываем loader
                        $loader.remove();
                        $mainBox.css('opacity', '1').css('pointer-events', 'all');

                        var data = JSON.parse(response);                        // Переформатируем из формата-json полученные данные
                        var htmlReviews = $('.reviews-container').html();       // Запоминаем все отзывы которые уже были показаны
                        $('.reviews-container').html(htmlReviews + data.text);  // Выводим сохраненные отзывы, добавляя еще одну страницу отзывов
                        vm.reviews.pageNumber = vm.reviews.pageNumber + 1;      // Увеличиваем индекс страницы на 1
                        if (!data.more) {       // Проверяем есть ли еще страницы
                            vm.reviews.showButtonMoreReviews = false;           // Скрываем кнопку "Показать еще"
                        }
                    },
                });
            },

            // Логи 2.0
            logVisit: function () {
                let vm = this;

                if (!vm.visitData.isBot) {
                    if (vm.visitData.visitUid) {
                        vm.ajaxAddVisitorLog();
                    } else {
                        vm.ajaxRestoreVisitUid();
                    }
                }
            },
            // Метод для восстановления visitUID из localStorage, либо генерация нового visitUID
            ajaxRestoreVisitUid: function () {
                let vm = this;
                let visitUid = localStorage.getItem('visit_uid') ? localStorage.getItem('visit_uid') : null;
                let action = visitUid ? 'restore' : 'generate';

                $.ajax({
                    type: 'post',
                    url: '/visitor-log/ajax-restore-visit-uid',
                    data: {
                        _csrf: yii.getCsrfToken(),
                        action: action,
                        visitUid: visitUid,
                    },
                    success: function (response) {
                        vm.visitData.visitUid = response.visitUid;
                        vm.visitData.visitUidString = response.visitUidString;
                        vm.ajaxAddVisitorLog();
                    },
                });
            },
            // Метод для записи лога
            ajaxAddVisitorLog: function () {
                let vm = this;
                let location = localStorage.region + ' (авто-определение: ' + localStorage.regionAuto + ')';
                localStorage.setItem('visit_uid', vm.visitData.visitUid);

                $.ajax({
                    type: 'post',
                    url: '/visitor-log/ajax-add-log',
                    data: {
                        _csrf: yii.getCsrfToken(),
                        visitUid: vm.visitData.visitUid,
                        clientLink: vm.visitData.clientLink,

                        ipAddress: vm.visitData.ipAddress,
                        location: location,

                        userAgent: vm.visitData.userAgent,
                        platform: vm.visitData.platform,
                        browserName: vm.visitData.browserName,
                        browserVersion: vm.visitData.browserVersion,

                        url: vm.visitData.url,
                        path: vm.visitData.path,
                        query: vm.visitData.query,

                        refUrl: vm.visitData.refUrl,
                        refHost: vm.visitData.refHost,
                        refPath: vm.visitData.refPath,
                        refQuery: vm.visitData.refQuery,

                        refType: vm.visitData.refType,
                        time: vm.visitData.time,
                    },
                    success: function (response) {
                        if (vm.isProductView) {
                            vm.defineQuickCode(vm.productId, vm.visitData.visitUid - 100000);
                        }
                    },
                });
            },

            defineQuickCode: function (productId, uidId, fromQv = false) {
                let vm = this;

                $.ajax({
                    type: 'get',
                    url: '/client-product-quick-code/ajax-define-quick-code',
                    data: {
                        productId: productId,
                        uidId: uidId,
                        refHost: vm.visitData.refHost,
                    },
                    success: function (response) {
                        if (response.success) {
                            if (fromQv) {
                                vm.qv.productQuickCode = vm.splitNumber(response.quickCode, 2, '-');
                            } else {
                                vm.productQuickCode = vm.splitNumber(response.quickCode, 2, '-');
                            }
                        }
                    },
                });
            },

            hideSovetnik: function() {
                var allDivs = document.getElementsByTagName("div");
                var count = 0;
                var found = false;
                for(i = 0;i < allDivs.length; i++)
                {
                    if (window.getComputedStyle(allDivs[i]).getPropertyValue("background-color") == "rgb(250, 223, 118)") {
                        var node = allDivs[i];
                        var html = document.getElementsByTagName("html");
                        while(node.parentNode.nodeName.toLowerCase() != "body" && count < 20){
                            node = node.parentNode;
                            count++;
                        }
                        node.setAttribute("style","display:none !important;");
                        html[0].setAttribute("style","margin-top: 0");
                    }
                }
            },
            copyTextById: function (id) {
                let emailLink = document.querySelector('#' + id);
                let range = document.createRange();
                range.selectNode(emailLink);
                window.getSelection().empty();
                window.getSelection().addRange(range);
                window.getSelection().toString();

                try {
                    let successful = document.execCommand('copy');
                    $('.btn-copy').attr('data-original-title', '');
                    if (successful) {
                        let btnCopy = $('.copy-' + id);
                        btnCopy.attr('data-original-title', 'скопирован');
                        btnCopy.mouseout();
                        btnCopy.mouseover();
                    }
                } catch (err) {
                    console.log('Ошибка при копировании');
                }
            }
        },
        computed: {
            // DEPRECATED
            // mainPhone: function () {
            //     if (this.refHost == 'alfa.kz') {
            //         return '8 727 356-36-05';
            //     } else if (this.refHost == 'tomas.kz') {
            //         return '8 727 356-36-04';
            //     } else if (this.refHost == 'geteml.com') {
            //         return '8 727 356-36-03';
            //     } else {
            //         return '8 727 356-36-00';
            //     }
            // },
            userRegion: function () {
                return this.otherRegion ? this.otherRegion : this.region;
            },
            priceFromOn: function () {
                return (this.query.tsena_ot && this.query.tsena_ot instanceof Array === false) ? true : false;
            },
            priceToOn: function () {
                return (this.query.tsena_do && this.query.tsena_do instanceof Array === false) ? true : false;
            },
            textOn: function () {
                return (this.searchText) ? true : false;
            },
            filtersOn: function () {
                let dfFiltersCheckboxLength = 0;

                // detecting if Dynamic filters on
                if (this.query.df !== undefined) {
                    for (var key in this.query.df.checkbox) {
                        dfFiltersCheckboxLength = dfFiltersCheckboxLength + this.query.df.checkbox[key].length;
                    }
                }

                if ((this.query.nalichie && this.query.nalichie === true) ||
                    this.priceFromOn ||
                    this.priceToOn ||
                    (this.query.brend && this.query.brend.length > 0) ||
                    (this.query.kategoriya instanceof Array === true && this.query.kategoriya && this.query.kategoriya.length > 0) ||
                    (dfFiltersCheckboxLength > 0) ||
                    (this.betweenFiltersOn.isOn) ||
                    (this.isPromoPage && this.query.kategoriya.length === 1)
                ) {
                    return true;
                }

                return false;
            },
            betweenFiltersOn: function () {
                var result = {
                    isOn: false,
                };

                if (this.query.df !== undefined) {
                    for (var key in this.query.df.between) {
                        result[key] = {};
                        if (this.query.df.between[key].ot != this.query.df.between[key].min) {
                            result[key].ot = true;
                            result.isOn = true;
                        } else {
                            result[key].ot = false;
                        }
                        if (this.query.df.between[key].do != this.query.df.between[key].max) {
                            result[key].do = true;
                            result.isOn = true;
                        } else {
                            result[key].do = false;
                        }
                    }
                }

                return result;
            },
            referHeight: function () {
                if (this.referSet.refersShow) {
                    return 'auto';
                } else {
                    return '32px';
                }
            },
            windowWidth: function () {
                return window.innerWidth;
            }
        },
        directives: {
            activateProductTab: {
                bind: function () {
                    let el = this.el,
                        $el = $(el);

                    $el.on('click', function (e) {
                        e.preventDefault();
                        // Забираем из href id цели, определяем активатор цели и активируем таб.
                        let $targetPill = $('[href="' + el.hash + '"]');
                        $targetPill.tab('show');
                    });

                    $el.on('shown.bs.tab', function () {
                        // Определяем смещение цели от верха.
                        let $target = $(el.hash),
                            offset = $target.offset().top;

                        // Cкроллим со смещением цели от верха и дополнительным смещением.
                        $('html, body').stop().animate({
                            scrollTop: Math.round(offset) - 200
                        }, 500);
                    });
                }
            },
            copyEToClipboard: {
                params: ['areaId'],

                bind: function () {
                    var $el = $(this.el),
                        vm = this.vm,
                        params = this.params;

                    $el.on('click', function () {
                        new Clipboard('.email_item.n_' + params.areaId, {
                            text: function() {
                                return $el.text().trim();
                            }
                        }).on('success', function () {
                            $el.tooltip('hide')
                              .attr('data-original-title', 'Скопировано!')
                              .tooltip('show');
                        });
                    });
                }
            },
            copyPToClipboard: {
                params: ['phoneId'],

                bind: function () {
                    var $el = $(this.el),
                        params = this.params;

                    $el.on('click', function () {
                        new Clipboard('.phone_item.n_' + params.phoneId, {
                            text: function() {
                                return $el.text().trim();
                            }
                        }).on('success', function () {
                            $el.tooltip('hide')
                              .attr('data-original-title', 'Скопировано!')
                              .tooltip('show');
                        });
                    });
                }
            },
            searchSubmit: {
                params: ['userIp', 'routeaPath'],

                bind: function () {
                    var vm = this.vm,
                        params = this.params,
                        $form = $(this.el);
                        // term = $form.find('input#search-term').first().val();

                    $form.on('beforeSubmit', function () {
                        if (!vm.searchTerm.replace(/\s/g, '')) {
                            return false;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/search/ajax-save-search-request',
                            data: {
                                term: vm.searchTerm,
                                userIp: params.userIp,
                                region: vm.region,
                                routePath: window.location.pathname + window.location.search,
                            },
                        });

                        return true;
                    });
                }
            },
            ajaxOrderSubmit: {
                bind: function () {
                    var vm = this.vm,
                        $form = $(this.el),
                        params = this.params;

                    $form.on('beforeSubmit', function () {
                        var loader = '<div class="loader la-timer la-2x"><div></div></div>';

                        $.ajax({
                            type: 'post',
                            url: '/products/ajax-order-submit',
                            data: $form.serialize(),
                            beforeSend: function () {
                                $form.find('button[type="submit"]').prop('disabled', true);
                                $form.find('input').prop('disabled', true);
                                $form.find('select').prop('disabled', true);
                                $form.css('opacity', '.3');
                                $form.closest('.modalbox-body').append(loader);
                            },
                            success: function (response) {
                                setTimeout(function () {
                                    $('body').scrollTo(0);
                                    $form.closest('.modalbox-body').find('.loader').remove();
                                    $form.find('button[type="submit"]').prop('disabled', false);
                                    $form.find('input').prop('disabled', false);
                                    $form.find('select').prop('disabled', false);
                                    $form.css('opacity', '1');

                                    if (response.success) {
                                        vm.orderFormStatus = 'success';
                                        // Цель Yandex.Metrika
                                        if (typeof ym === 'function') {
                                            ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_ORDER_FORM');
                                        }

                                        if (typeof roistat != 'undefined') {
                                            roistat.event.send('FORM_SUBMIT_BUY', {'product': vm.productName});
                                        }

                                        // Цель Google Analytics
                                        if (typeof gtag === 'function') {
                                            gtag('event', 'submit_order_form', {
                                                'event_category': 'forms',
                                            });
                                        }
                                    }
                                    if (!response.success) {
                                        vm.orderFormStatus = 'fail';
                                    }
                                }, 1000);
                            },
                            error: function () {
                                $('body').scrollTo(0);
                                $form.closest('.modalbox-body').find('.loader').remove();

                                vm.orderFormStatus = 'fail';
                            },
                        });

                        return false;
                    });
                }
            },
            ajaxCartOrderSubmit: {
                bind: function () {
                    var vm = this.vm,
                        $form = $(this.el),
                        params = this.params;

                    $form.on('beforeSubmit', function () {
                        var postData = $form.serializeArray(),
                            loader = '<div class="loader la-timer la-2x"><div></div></div>';

                        postData.push({name: 'cartProductsJson', value: JSON.stringify(vm.cartProducts)});

                        $.ajax({
                            type: 'post',
                            url: '/cart/ajax-cart-order-submit',
                            data: postData,
                            beforeSend: function () {
                                $form.find('button[type="submit"]').prop('disabled', true);
                                $form.find('input').prop('disabled', true);
                                $form.find('select').prop('disabled', true);
                                $form.css('opacity', '.3');
                                $form.closest('.modalbox-body').append(loader);
                            },
                            success: function (response) {
                                setTimeout(function () {
                                    $('body').scrollTo(0);
                                    $form.closest('.modalbox-body').find('.loader').remove();
                                    $form.find('button[type="submit"]').prop('disabled', false);
                                    $form.find('input').prop('disabled', false);
                                    $form.find('select').prop('disabled', false);
                                    $form.css('opacity', '1');

                                    if (response.success) {
                                        vm.orderFormStatus = 'success';
                                        // Цель Yandex.Metrika
                                        if (typeof ym === 'function') {
                                            ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_CART_FORM');
                                        }

                                        if (typeof roistat != 'undefined') {
                                            roistat.event.send('FORM_SUBMIT_CART');
                                        }

                                        // Цель Google Analytics
                                        if (typeof gtag === 'function') {
                                            gtag('event', 'submit_cart_form', {
                                                'event_category': 'forms',
                                            });
                                        }
                                    }
                                    if (!response.success) {
                                        vm.orderFormStatus = 'fail';
                                    }
                                }, 1000);
                            },
                            error: function () {
                                $('body').scrollTo(0);
                                $form.closest('.modalbox-body').find('.loader').remove();

                                vm.orderFormStatus = 'fail';
                            },
                        });

                        return false;
                    });
                }
            },
            ajaxDiscountSubmit: {
                bind: function () {
                    var vm = this.vm,
                        $form = $(this.el),
                        params = this.params;

                    $form.on('beforeSubmit', function () {
                        var loader = '<div class="loader la-timer la-2x"><div></div></div>';

                        $.ajax({
                            type: 'post',
                            url: '/products/ajax-order-submit',
                            data: $form.serialize(),
                            beforeSend: function () {
                                $form.find('button[type="submit"]').prop('disabled', true);
                                $form.find('input').prop('disabled', true);
                                $form.find('select').prop('disabled', true);
                                $form.css('opacity', '.3');
                                $form.closest('.modalbox-body').append(loader);
                            },
                            success: function (response) {
                                setTimeout(function () {
                                    $('body').scrollTo(0);
                                    $form.closest('.modalbox-body').find('.loader').remove();
                                    $form.find('button[type="submit"]').prop('disabled', false);
                                    $form.find('input').prop('disabled', false);
                                    $form.find('select').prop('disabled', false);
                                    $form.css('opacity', '1');

                                    if (response.success) {
                                        vm.discountFormStatus = 'success';
                                        // Цель Yandex.Metrika
                                        if (typeof ym === 'function') {
                                            ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_DISCOUNT_FORM');
                                        }

                                        if (typeof roistat != 'undefined') {
                                            roistat.event.send('FORM_SUBMIT_DISCOUNT', {'product': vm.productName});
                                        }

                                        // Цель Google Analytics
                                        if (typeof gtag === 'function') {
                                            gtag('event', 'submit_discount_form', {
                                                'event_category': 'forms',
                                            });
                                        }
                                    }
                                    if (!response.success) {
                                        vm.discountFormStatus = 'fail';
                                    }
                                }, 1000);
                            },
                            error: function () {
                                $('body').scrollTo(0);
                                $form.closest('.modalbox-body').find('.loader').remove();

                                vm.discountFormStatus = 'fail';
                            },
                        });

                        return false;
                    });
                }
            },
            ajaxConsultSubmit: {
                bind: function () {
                    var vm = this.vm,
                        $form = $(this.el),
                        params = this.params;

                    $form.on('beforeSubmit', function () {
                        var loader = '<div class="loader la-timer la-2x"><div></div></div>';

                        $.ajax({
                            type: 'post',
                            url: '/products/ajax-order-submit',
                            data: $form.serialize(),
                            beforeSend: function () {
                                $form.find('button[type="submit"]').prop('disabled', true);
                                $form.find('input').prop('disabled', true);
                                $form.find('select').prop('disabled', true);
                                $form.css('opacity', '.3');
                                $form.closest('.modalbox-body').append(loader);
                            },
                            success: function (response) {
                                setTimeout(function () {
                                    $('body').scrollTo(0);
                                    $form.closest('.modalbox-body').find('.loader').remove();
                                    $form.find('button[type="submit"]').prop('disabled', false);
                                    $form.find('input').prop('disabled', false);
                                    $form.find('select').prop('disabled', false);
                                    $form.css('opacity', '1');

                                    if (response.success) {
                                        vm.consultFormStatus = 'success';
                                        // Цель Yandex.Metrika
                                        if (typeof ym === 'function') {
                                            ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_CONSULT_FORM');
                                        }

                                        if (typeof roistat != 'undefined') {
                                            roistat.event.send('FORM_SUBMIT_CONSULT', {'product': vm.productName});
                                        }

                                        // Цель Google Analytics
                                        if (typeof gtag === 'function') {
                                            gtag('event', 'submit_consult_form', {
                                                'event_category': 'forms',
                                            });
                                        }
                                    }
                                    if (!response.success) {
                                        vm.consultFormStatus = 'fail';
                                    }
                                }, 1000);
                            },
                            error: function () {
                                $('body').scrollTo(0);
                                $form.closest('.modalbox-body').find('.loader').remove();

                                vm.consultFormStatus = 'fail';
                            },
                        });

                        return false;
                    });
                }
            },
            putToCartAjax: {
                params: ['productId', 'isPromo'],

                bind: function () {
                    var $el = $(this.el),
                        vm = this.vm,
                        params = this.params;

                    $el.on('click', function () {
                        var inQv = false;

                        if (!$el.data('product-id')) {
                            params.productId = vm.qv.product.id;
                            params.isPromo = vm.qv.product.is_promo;
                            params.isSupportNeeded = vm.qv.product.isSupportNeeded;

                            inQv = true;
                        } else {
                            params.productId = $el.data('product-id');
                            params.isPromo = $el.data('is-promo');
                            params.isSupportNeeded = vm.isSupportNeeded;
                        }

                        if (inQv) {
                            vm.qv.successAdded = false;
                        } else {
                            vm.successAdded = false;
                        }

                        $.ajax({
                            type: 'post',
                            url: '/cart/ajax-put-to-cart',
                            data: {
                                productId: params.productId,
                                isPromo: params.isPromo,
                                isSupportNeeded: params.isSupportNeeded,
                            },
                            success: function (response) {
                                if (response.success === true) {
                                    vm.cart.count = response.count;
                                    vm.cart.cost = response.cost;

                                    var duration = 1500;

                                    if (inQv) {
                                        vm.qv.successAdded = true;

                                        if ('timerSuccessAdded' in vm.qv) {
                                            clearTimeout(vm.qv.timerSuccessAdded);
                                        }
                                        vm.qv.timerSuccessAdded = setTimeout(function() {
                                            vm.qv.successAdded = false;
                                        }, duration);

                                    } else {
                                        vm.successAdded = true;

                                        if ('timerSuccessAdded' in vm) {
                                            clearTimeout(vm.timerSuccessAdded);
                                        }

                                        vm.timerSuccessAdded = setTimeout(function() {
                                            vm.successAdded = false;
                                        }, duration);
                                    }

                                    // Цель Yandex.Metrika
                                    if (typeof ym === 'function') {
                                        ym(vm.yaMetrikaId, 'reachGoal', 'PRODUCT_ADD_TO_CART_BTN');
                                    }

                                    // Цель Google Analytics
                                    if (typeof gtag === 'function') {
                                        gtag('event', 'click_add_to_cart_btn', {
                                            'event_category': 'buttons',
                                        });
                                    }
                                }
                            }
                        });
                    });
                }
            },
            removeFromCartAjax: {
                params: ['productId'],

                bind: function () {
                    var $el = $(this.el),
                        vm = this.vm,
                        params = this.params;

                    $el.on('click', function () {
                        $.ajax({
                            type: 'post',
                            url: '/cart/ajax-remove-from-cart',
                            data: {
                                productId: params.productId,
                            },
                            success: function (response) {
                                if (response.success === true) {
                                    if (response.count === 0) {
                                        vm.isCartEmpty = true;
                                    }
                                    Vue.delete(vm.cartProducts, params.productId);
                                    // vm.cartProducts[params.productId] = [];
                                    vm.cart.count = response.count;
                                    vm.cart.cost = response.cost;
                                }
                            }
                        });
                    });
                }
            },
            watchQuantity: {
                params: ['productId'],

                update: function (val, oldVal) {
                    var vm = this.vm,
                        params = this.params;

                    if (oldVal !== undefined && val != oldVal) {
                        var quantity = parseInt(val);

                        // vm.cartProducts[params.productId].quantity = quantity;

                        $.ajax({
                            type: 'post',
                            url: '/cart/ajax-change-product-quantity',
                            data: {
                                productId: params.productId,
                                quantity: quantity,
                            },
                            success: function (response) {
                                if (response.success) {
                                    if (response.count !== 0) {
                                        vm.cartProducts[params.productId].productCost = response.productCost;
                                    } else if (response.count === 0) {
                                        vm.cartProducts[params.productId] = [];
                                        vm.isCartEmpty = true;
                                    }

                                    vm.cart.count = response.count;
                                    vm.cart.cost = response.cost;
                                }
                                // if (!response.success) {

                                // }
                            },
                            // error: function () {

                            // },
                        });
                    }
                }
            },
            autocomplete: {
                params: ['ajaxUrl', 'baseUrl', 'zone'],

                bind: function () {
                    var $el = $(this.el),
                        vm = this.vm,
                        params = this.params;

                    $el.autocomplete({
                        source: params.ajaxUrl,
                        minLength: 2,
                        search: function(event, ui) {

                        },
                        response: function (event, ui) {

                        },
                        open: function (event, ui) {
                            var term = vm.searchTerm.trim().replace(/\s/g, '+'),
                                resultsUrl = params.baseUrl + '/search?term=' + term;

                            $('.catalog_btn').on('mouseenter', function () {
                                $el.autocomplete('close');
                            });

                            $('.ui-autocomplete').append('<li><a class="see-more" href="' + resultsUrl + '">Все результаты подробнее</a></li>');
                        },
                        focus: function (event, ui) {
                            return false;
                        },
                        close: function (event, ui) {
                            vm.overlay = false;
                        },
                        select: function (event, ui) {
                            var item = ui.item,
                                productFullname = item.cat_name_single + ' ' + item.vendor + ' ' + item.name + ' (' + item.part_number + ')';

                            vm.searchTerm = productFullname;
                            $('.ui-autocomplete').find('#' + ui.item.id)[0].click();
                        },
                    });
                    $el.autocomplete( 'instance' )._resizeMenu = function( ul, items ) {
                        this.menu.element.outerWidth( 707 );
                    };
                    $el.autocomplete( 'instance' )._renderMenu = function( ul, items ) {
                        // if (items.length === 1) {
                        //     $('.ui-autocomplete').remove();
                        //
                        //     var item = items[0],
                        //         url = params.baseUrl + '/tovar/' + item.id + '/' + item.alias;
                        //
                        //     window.location.href = url;
                        // } else {
                            var that = this;

                            vm.overlay = true;

                            $.each( items, function( index, item ) {
                                that._renderItemData( ul, item );
                            });
                        // }
                    };
                    $el.autocomplete( 'instance' )._renderItem = function( ul, item ) {
                        var productFullname = item.cat_name_single + ' ' + item.vendor + ' ' + item.name + ' (' + item.part_number + ')',
                            url = params.baseUrl + '/tovar/' + item.id + '/' + item.alias,
                            img = 'https://static.apltech.' + params.zone + (item.firstImage ? item.firstImage.path.replace('size', '150') : '/uploads/products/placeholder@150.png');
                        return $( '<li>' )
                            .append('<div class="img_wrapper"><img height="30" src=' + img + '><div>')
                            .append( '<a href=' + url + ' id=' + item.id + '>' + productFullname + '</a>' )
                            .appendTo( ul );
                    };

                },
            },
            searchExample: {
                bind: function () {
                    var $el = $(this.el),
                        vm = this.vm,
                        params = this.params,
                        $searchInputEl = $('#search-term');

                    vm.searchExampleInterval = setInterval(function() {
                        var rand = Math.floor(Math.random() * vm.searchExamples.length);

                        vm.searchExample = vm.searchExamples[rand];
                    }, 10000);

                    $el.on('click', function () {
                        clearInterval(vm.searchExampleInterval);
                        vm.searchTerm = vm.searchExample;
                        $searchInputEl.autocomplete('search', vm.searchTerm);
                        $searchInputEl[0].focus();
                    });
                }
            },
            smartMenu: {
                bind: function () {
                    var $el = $(this.el);

                    $el.menuAim({
                        activate: function (row) {
                            var $row = $(row),
                                $parent = $row.closest('ul'),
                                $submenu = $row.find('.submenu'),
                                height = $parent.height(),
                                subHeight = $submenu.height(),
                                diff = height - subHeight;

                            if ( diff > 0 ) {
                                $submenu.height(height);
                            } else if ( diff < 0 ) {
                               $parent.height(subHeight);
                            }

                            $row.addClass('active');
                            $submenu.show();
                        },
                        deactivate: function (row) {
                            var $row = $(row),
                                $parent = $row.closest('ul'),
                                $submenu = $row.find('.submenu');

                            $parent.height('auto');

                            $row.removeClass('active');
                            $submenu.hide();
                        },
                        exitMenu: function (row) {
                            var $parent = $(row);

                            $parent.find('li.active').first().removeClass('active');
                            $parent.find('.submenu').hide();
                            $parent.height('auto');
                        }
                    });
                },
            },
            owlProductPhotoSlider: {
                bind: function () {
                    let vm = this.vm,
                        $owlCarousel = $(this.el);

                    $owlCarousel.owlCarousel({
                        autoPlay: 15000,
                        items : 1,
                        itemsCustom : [1200, 1],
                        mouseDrag: true,
                        pagination: true,
                        stopOnHover: true,
                        afterUpdate: function (elem) {
                            vm.setSliderPagination($(elem));
                        },
                        afterInit: function (elem) {
                            vm.setSliderPagination($(elem));
                        },
                    });
                }
            },
            owlVendorSlider: {
                bind: function () {
                    var $el = $(this.el);

                    $el.owlCarousel({
                        autoPlay: 4000,

                        items : 5,

                        mouseDrag: true,
                        pagination: true,
                        navigation: false,
                        stopOnHover: true
                    });
                }
            },
            owlMainSlider: {
                bind: function () {
                    var $el = $(this.el);

                    $el.owlCarousel({
                        autoPlay: 4000,

                        // items : 1,
                        // itemsDesktop : [1199,3],
                        // itemsDesktopSmall : [770,1],

                        itemsCustom: [
                            [375, 2],
                            [768, 1]
                        ],

                        mouseDrag: true,
                        pagination: true,
                        navigation: false,
                        stopOnHover: true
                    });
                },
            },
            owlProductSlider: {
                bind: function () {
                    var $el = $(this.el);

                    $el.owlCarousel({
                        autoPlay: false,

                        itemsCustom : [
                            [320, 1],
                            [375, 2],
                            [768, 3],
                            [1024, 4]
                        ],

                        mouseDrag: false,
                        pagination: false,
                        navigation: true,
                        stopOnHover: true,
                        navigationText: [
                            '<i class="fa fa-angle-left"></i>',
                            '<i class="fa fa-angle-right"></i>',
                        ]
                  });
                },
            },
            owlPromoSlider: {
                bind: function () {
                    var $el = $(this.el);

                    $el.owlCarousel({
                        autoPlay: 4000,

                        items : 1,

                        mouseDrag: true,
                        pagination: false,
                        navigation: false,
                        navigationText: [
                            '<i class="fa fa-angle-left"></i>',
                            '<i class="fa fa-angle-right"></i>',
                        ],
                        stopOnHover: true
                    });
                },
                update: function (val, oldVal) {
                    var $el = $(this.el);

                    if (oldVal !== undefined) {
                        if (val === false) {
                            $el.data('owlCarousel').stop();
                        } else if (val === true) {
                            $el.data('owlCarousel').play();
                        }
                    }
                }
            },
            productGallerySlider: {
                bind: function () {
                    var $el = $(this.el);

                    $el.show().bxSlider({
                        auto: true,
                        stopAutoOnClick: true,
                        pagerCustom: '#bx-pager',
                        controls: false,
                        autoHover: true
                    });
                }
            },
            productGalleryBigSlider: {
                bind: function () {
                    var vm = this.vm;
                    var $el = $(this.el);

                    vm.bs.$bxSlider = $el.bxSlider({
                        auto: true,
                        stopAutoOnClick: true,
                        pagerCustom: '.bs-bx-pager',
                        controls: false,
                        autoHover: true
                    });
                }
            },
            loupe: {
                bind: function() {
                    var vm = this.vm;
                    var $el = $(this.el);

                    if ($el.hasClass('loupe')) {
                        $el.find('.img img').on('click', function() {
                            vm.bsShow();
                        });
                    }
                }
            },
            zoom: {
                bind: function() {
                    let vm = this.vm,
                        $el = $(this.el);

                    if (vm.isMobile()) {
                        return;
                    }

                    $el.find('.zoom-wrapper').each(function() {
                        let $wrapper = $(this);
                        let src = $wrapper.data('src');

                        $wrapper.zoom({
                            url: src,
                            deadZone: 200,
                            on: 'click',
                            onZoomIn: function () {
                                $wrapper.addClass('zoom-in');
                            },
                            onZoomOut: function () {
                                $wrapper.removeClass('zoom-in');
                            }
                        });
                    });
                }
            },
            'autofocusOnClick': function (value) {
                var $el = $(this.el);

                $el.on('click', function () {
                    setTimeout(function() {
                        $(value).focus();
                    }, 1);
                });
            },
            smartAutofocus: {
                bind: function () {
                    var vm = this.vm,
                        $el = $(this.el),
                        $orderForm = $('#orderForm');

                    $el.on('click', function () {
                        var $inputName = $orderForm.find('input:text[name="ContactForm[name]"]');
                        setTimeout(function () {
                            $inputName[0].focus();
                            return false;
                            // $inputs.each(function (index, item) {
                            //     var $item = $(item);
                            //     if ( $item.val().length === 0 ) {
                            //         $item[0].focus();
                            //         return false;
                            //     }
                            // });
                        }, 1);
                    });
                }
            },
            smartAutofocusQv: {
                bind: function () {
                    var vm = this.vm,
                        $el = $(this.el),
                        $orderForm = $('#oneClickOrderForm');

                    $el.on('click', function () {
                        var $inputName = $orderForm.find('input:text[name="ContactForm[name]"]');
                        setTimeout(function () {
                            $inputName[0].focus();
                            return false;
                            // $inputs.each(function (index, item) {
                            //     var $item = $(item);
                            //     if ( $item.val().length === 0 ) {
                            //         $item[0].focus();
                            //         return false;
                            //     }
                            // });
                        }, 1);
                    });
                }
            },
            smartAutofocusDiscountForm: {
                bind: function () {
                    var vm = this.vm,
                        $el = $(this.el),
                        $orderForm = $('#oneClickDiscountForm');

                    $el.on('click', function () {
                        var $inputName = $orderForm.find('input:text[name="ContactForm[name]"]');
                        setTimeout(function () {
                            $inputName[0].focus();
                            return false;
                            // $inputs.each(function (index, item) {
                            //     var $item = $(item);
                            //     if ( $item.val().length === 0 ) {
                            //         $item[0].focus();
                            //         return false;
                            //     }
                            // });
                        }, 1);
                    });
                }
            },
            smartAutofocusConsultForm: {
                bind: function () {
                    var vm = this.vm,
                        $el = $(this.el),
                        $orderForm = $('#oneClickConsultForm');

                    $el.on('click', function () {
                        var $inputName = $orderForm.find('input:text[name="ContactForm[name]"]');
                        setTimeout(function () {
                            $inputName[0].focus();
                            return false;
                            // $inputs.each(function (index, item) {
                            //     var $item = $(item);
                            //     if ( $item.val().length === 0 ) {
                            //         $item[0].focus();
                            //         return false;
                            //     }
                            // });
                        }, 1);
                    });
                }
            },
            phoneMask: {
                bind: function () {
                    var $el = $(this.el);

                    $el.mask("8 (999) 999-9999");
                }
            },
            readmore: {
                bind: function () {
                    var $el = $(this.el);

                    $el.readmore({
                        moreLink: '<a href="#">Показать описание полностью</a>',
                        lessLink: '<a href="#">Скрыть полное описание</a>'
                    });
                }
            },
            minHeightHandle: {
                update: function (productsHtml) {
                    var $el = $(this.el);

                    setTimeout(function() {
                        $el.css('min-height', $el.height());
                    }, 1);
                }
            },
            imageLoad: {
                bind: function () {
                    let $el = $(this.el);

                    setTimeout(function() {
                        $el.show();
                    }, 500);
                }
            },
            mMenu: {
                bind: function() {
                    $(this.el).mmenu({
                        navbar: {
                            title: 'Каталог товаров'
                        },
                        // searchfield: {
                        //     placeholder: 'Поиск по каталогу',
                        //     search: false
                        // },
                        navbars: [
                            {
                                position: "top",
                                // content: [
                                //     "searchfield"
                                // ]
                            }
                        ]
                    }, {
                        offCanvas: {
                            pageSelector: '.main-content'
                        },
                        // searchfield: {
                        //     input: {
                        //         name: 'term'
                        //     },
                        //     form: {
                        //         action: '/search'
                        //     },
                        //     submit: true
                        // }
                    });
                }
            },
            mMenuFilterSort: {
                bind: function() {
                    var vm = this.vm;

                    $(this.el).mmenu({
                        offCanvas: {
                            position: 'right'
                        },
                        navbar: {
                            title: 'Сортировка'
                        }
                    }, {
                        offCanvas: {
                            pageSelector: '.main-content'
                        }
                    });

                    $('.m-filter-sort input').on('click', function() {
                        vm.hideFilterSort();
                    });
                }
            },
            mMenuFilterFilters: {
                bind: function() {
                    $(this.el).mmenu({
                        navbar: {
                            title: 'Фильтры'
                        }
                    }, {
                        offCanvas: {
                            pageSelector: '.main-content'
                        }
                    });
                }
            },
            quickCodeInfoPopover: {
                bind: function () {
                    let $el = $(this.el),
                        expression = this.expression;

                    let popover = '<div style="display: flex; flex-direction: column">' +
                            '<span style="display: block;font-size: 14px;font-weight: bold;">Ваш код для этого товара.</span>' +
                            '<span style="display: block;font-size: 14px;font-weight: bold;">Сообщите код менеджеру по&nbsp;телефону.</span>' +
                            '<span style="display: block;font-size: 12px">Код обновляется каждые 12&nbsp;часов.</span>' +
                    '</div>';

                    $el.popover({
                        container: expression.container,
                        content: popover,
                        html: true,
                    });

                    $el.on('show.bs.popover', function () {
                        $el.addClass('hovered');
                    });

                    $el.on('hide.bs.popover', function () {
                        $el.removeClass('hovered');
                    });
                }
            },
        },
        watch: {
            clientType: function (val) {
                $.ajax({
                    type: 'post',
                    url: '/cart/ajax-change-client-type',
                    data: {
                        clientType: val,
                    },
                    success: function (response) {
                        if (response.success) {
                            vm.cartProducts = response.cartProducts;
                            vm.cart.count = response.count;
                            vm.cart.cost = response.cost;
                        }
                    }
                });
            },
            // query: {
            //     handler: function (val, oldVal) {
            //         var vm = this,
            //             loader = '<div class="la-timer la-2x"><div></div></div>';

            //         $.ajax({
            //             type: 'post',
            //             url: vm.ajaxUrl,
            //             data: {
            //                 route: vm.route,
            //                 routeParams: location.search.replace('?',''),
            //                 query: JSON.stringify(vm.query),
            //             },
            //             beforeSend: function () {
            //                 vm.ajaxProductsHtml = loader;
            //             },
            //             success: function (response) {
            //                 vm.ajaxProductsHtml = response.html;
            //                 vm.ajaxNoProducts = response.hasOwnProperty('ajaxNoProducts') ? true : false;
            //                 vm.minPrice = response.minPrice;
            //                 vm.maxPrice = response.maxPrice;
            //                 vm.productsCount = response.productsCount;

            //                 // Vue.set(vm.query.df, 'between', response.dfBetween);
            //                 // Vue.set(vm.dfBetweenRanges, 0, response.dfBetweenRanges);

            //                 // // changing url query params
            //                 // window.history.replaceState('', '', '?' + $.param(vm.query));
            //                 // // change Between filter slider values
            //                 // for (var key in val.df.between) {
            //                 //     $('.slider_range.' + key).slider( 'option', 'values', [ val.df.between[key].ot, val.df.between[key].do ] );
            //                 // }
            //             }
            //         });
            //     },
            //     deep: true,
            // }

        },
    });
