<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">

        <div class="user-profile">
            <div class="ulogo">
                <a href="/admin">
                    <!-- logo for regular state and mobile devices -->
                    <div class="d-flex align-items-center justify-content-center">
                        <img src="/images/logo-dark.png" alt="">
                        <h3><b>Fatiha</b> Admin</h3>
                    </div>
                </a>
            </div>
        </div>

        <!-- sidebar menu-->
        <ul class="sidebar-menu" data-widget="tree">

            <li class="@if(request()->is('*translation')) active @endif">
                <a href="{{ route('admin.translation.index') }}">
                    <i data-feather="pie-chart"></i>
                    <span>Трансляции</span>
                </a>
            </li>
            <li class="@if(request()->is('*translation_request*')) active @endif">
                <a href="{{ route('admin.translation_request.index') }}">
                    <i data-feather="pie-chart"></i>
                    <span>Заявки</span>
                </a>
            </li>
            <li class="header nav-small-cap">Дополнительно</li>

            <li>

                <a href="{{ route('logout') }}"
                   onclick="event.preventDefault();
document.getElementById('logout-form').submit();">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                    <i data-feather="lock"></i>
                    <span>Выход</span>
                </a>
            </li>

        </ul>
    </section>

    {{--<div class="sidebar-footer">--}}

    {{--</div>--}}
</aside>