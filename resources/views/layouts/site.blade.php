<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="description" content="Рухани даму. Табыс табу. Жетіктікке жету.">
    <meta name="keywords" content="Курсы">
    <meta name="author" content="Jaslan">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Fatiha Center</title>
    <link rel="icon" type="image/x-icon" href="/ICON.png">
    <!-- Mobile Specific Meta-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- bootstrap.min css -->
    <link rel="stylesheet" href="assets/vendors/bootstrap/bootstrap.css">
    <!-- Iconfont Css -->
    <link rel="stylesheet" href="assets/vendors/awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/vendors/flaticon/flaticon.css">
    <link rel="stylesheet" href="assets/fonts/gilroy/font-gilroy.css">
    <link rel="stylesheet" href="assets/vendors/magnific-popup/magnific-popup.css">
    <!-- animate.css -->
    <link rel="stylesheet" href="assets/vendors/animate-css/animate.css">
    <link rel="stylesheet" href="assets/vendors/animated-headline/animated-headline.css">
    <link rel="stylesheet" href="assets/vendors/owl/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/vendors/owl/assets/owl.theme.default.min.css">
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="assets/css/woocomerce.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">

    <script>
        window.onload = function (){

            const queryString = window.location.search;
            const urlParams = new URLSearchParams(queryString);
            let id = urlParams.get('id');

            if(id)
                localStorage.setItem('tid',id);

            let tid = localStorage.getItem('tid');

            if(tid== null){
                document.getElementById('authButton').style.display = 'block';
            }else{
                document.getElementById('accountButton').style.display = 'block';
            }
        }
    </script>

</head>

<body id="top-header">



<header style="border-bottom-width: 0.1px; border-bottom-style: solid; border-bottom-color: #031f42;" class="header-style-1">
    <div class="header-topbar topbar-style-1">
        <div class="container">
            <div class="row justify-content-between align-items-center">
                <div class="col-xl-8 col-lg-8 col-sm-6">
                    <!-- <div class="header-contact text-center text-sm-start text-lg-start">
                          <a href="#">050000 Казахстан, Алматы</a>
                     </div>-->
                </div>
            </div>
        </div>
    </div>

    <div class="header-navbar navbar-sticky">
        <div class="container">
            <div class="d-flex align-items-center justify-content-between">
                <div class="site-logo">
                    <a href="https://fatiha.center">
                        <img src="assets/img/logo.png" alt="" class="img-fluid" />
                    </a>
                </div>

                <div class="offcanvas-icon d-block d-lg-none">
                    <a href="#" class="nav-toggler"><i class="fal fa-bars"></i></a>
                </div>

                <nav class="site-navbar ms-auto">
                    <ul class="primary-menu">
                        <li class="current">
                            <a style="text-transform: inherit;" href="https://fatiha.center">Басты бет</a>

                        </li>
                        <li>
                            <a href="https://fatiha.center/courses.php">Курстар</a>

                        </li>
                        <li><a style="text-transform: inherit;" href="/">Тікелей эфир</a></li>



                        <li>
                            <a href="https://fatiha.center/contact.php">Байланыс</a>
                        </li>
                        <li>
                            <div >
                                <div  id="authButton" style="display:none; margin-left:10px; margin-top:15px;" >
                                    @if(!\Illuminate\Support\Facades\Route::is('site.translation.show'))
                                    <script async src="https://telegram.org/js/telegram-widget.js?21" data-telegram-login="live_fatiha_bot" data-size="medium" data-radius="8" data-auth-url="/" data-request-access="write"></script>
                                        @endif
                                </div>

                                <a id="accountButton" style="display:none;" href="https://fatiha.center/profile"> <i class="fa fa-user-alt"></i> Аккаунт</a>
                            </div>

                        </li>
                    </ul>

                    <a href="#" class="nav-close"><i class="fal fa-times"></i></a>
                </nav>







            </div>
        </div>
    </div>
</header>

@yield('content')

<!-- Footer section start -->
<section class="footer footer-4">
    <div class="footer-mid">
        <div class="container">
            <div class="row">
                <div class="col-xl-3 me-auto col-sm-8">
                    <div class="footer-logo mb-3">
                        <img style="    width: 150px;" src="assets/img/logo-white.png" alt="" class="img-fluid">
                    </div>
                    <div class="widget footer-widget mb-5 mb-lg-0">
                        <p style="font-size: 20px;">Рухани даму. Табыс табу. Жетіктікке жету.</p>
                    </div>
                </div>

                <div class="col-xl-2 col-sm-4">
                    <div class="footer-widget mb-5 mb-lg-0">
                        <ul class="list-unstyled footer-links">
                            <li><a style="text-transform: inherit; font-size: 20px;" href="fatiha.center/">Басты бет</a></li>
                            <li><a style="text-transform: inherit; font-size: 20px;" href="fatiha.center/about.php" >Біз жайлы</a></li>
                            <li><a style="font-size: 20px;" href="fatiha.center/courses.php">Курстар</a></li>
                            <li><a style="font-size: 20px;" href="fatiha.center/contact.php">Байланыс</a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-xl-2 col-sm-4">
                    <div class="footer-widget mb-5 mb-lg-0">
                        <ul class="list-unstyled footer-links">
                            <li><h5 class="text-white">Email</h5><a style="font-size: 20px;" href="#">info@fatiha.center</a></li>
                        </ul>
                        <!--	<div class="footer-socials mt-4">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                            </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-btm">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-6 col-sm-12 col-lg-6">
                    <p class="mb-0 copyright text-sm-center text-lg-start">© 2022 Барлық құқықтар сақталған</p>
                </div>

            </div>
        </div>
    </div>

    <div class="fixed-btm-top">
        <a href="#top-header" class="js-scroll-trigger scroll-to-top"><i class="fa fa-angle-up"></i></a>
    </div>

</section>
<!-- Footer section End -->






<!--
Essential Scripts
=====================================-->

<!-- Main jQuery -->
<script src="assets/vendors/jquery/jquery.js"></script>
<script type='text/javascript' src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<!-- Bootstrap 5:0 -->
<script src="assets/vendors/bootstrap/popper.min.js"></script>
<script src="assets/vendors/bootstrap/bootstrap.js"></script>
<!-- Counterup -->
<script src="assets/vendors/counterup/waypoint.js"></script>
<script src="assets/vendors/counterup/jquery.counterup.min.js"></script>
<!--  Owl Carousel -->
<script src="assets/vendors/owl/owl.carousel.min.js"></script>
<!-- Isotope -->
<script src="assets/vendors/isotope/jquery.isotope.js"></script>
<script src="assets/vendors/isotope/imagelaoded.min.js"></script>
<!-- Animated Headline -->
<script src="assets/vendors/animated-headline/animated-headline.js"></script>
<!-- Magnific Popup -->
<script src="assets/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>

<script src="assets/js/script.js?v=2"></script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>

</body>
</html>