@extends('layouts.site')
@section('title', '- '  )

@section('content')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v15.0" nonce="iq2vFGbl"></script>
    <section class="galym-page-header page-header-2" style="background-image: url({{ $item->background }}) !important;">

        <div class="container">
            <div class="row">
                <div class="col-lg-7 col-xl-7">
                    <div class="course-header-wrapper mb-0 bg-transparent">
                        @if((new \Jenssegers\Agent\Agent())->isMobile())
                            <h1 style="font-family: var(--bs-font-sans-serif) !important;" class="mb-3">&nbsp;</h1>
                            <h1 style="font-family: var(--bs-font-sans-serif) !important;" class="mb-1">&nbsp;</h1>
                            <p style="font-family: var(--bs-font-sans-serif) !important;">&nbsp;</p>
                        @else
                            <h1 style="font-family: var(--bs-font-sans-serif) !important;" class="mb-3">{{ $item->name }}</h1>
                            <p style="font-family: var(--bs-font-sans-serif) !important;">{{ substr($item->description, 0, 80) }}...</p>
                        @endif
                    </div>
                </div>

                <div class="col-xl-5">
                    <div class="course-thumb-wrap">
                        <div class="course-thumbnail mb-0">
                            <img src="/{{ $item->image }}" alt="" class="img-fluid w-100">
                        </div>
                        @if($item->access)
                            <a class="popup-video video_icon" href="{{ strpos($item->link, 'https://') !== false ? url($item->link) : url('https://' . $item->link) }}">
                                <i class="fal fa-play"></i>
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="tutori-course-single tutori-course-layout-3 page-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-xl-8 col-lg-7">
                    <div class="tutori-course-content">

                        <div class="tab-content tutori-course-content" id="nav-tabContent">
                            <div class="single-course-details ">
                                <h4 style="font-family: var(--bs-font-sans-serif) !important;" class="course-title">{{ $item->name }}</h4>
                                <p style="font-size: 17px; font-family: var(--bs-font-sans-serif) !important;">
                                    {{ $item->description }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-5">
                    <!-- Course Sidebar start -->
                    <div class="course-sidebar course-sidebar-5 mt-5 mt-xl-0">
                        <div class="course-widget course-details-info">
                            <div class="price-header">
                                <h2 style="font-family: var(--bs-font-sans-serif) !important;" class="course-price">{{ $item->price ? 'Ақылы' : 'Тегін' }}<span></span></h2>
                                <span class="course-price-badge onsale"></span>
                            </div>
                            <ul class="course-sidebar-list">
                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span style="font-family: var(--bs-font-sans-serif) !important;"><i class="far fa-clock"></i>Уақыт</span>
                                        {{ \Carbon\Carbon::createFromFormat('H:i:s', $item->time)->format('H:i') }}
                                    </div>
                                </li>
                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="far fa-globe"></i>Тіл</span>
                                        {{ $item->language }}
                                    </div>
                                </li>

                                <li>
                                    <div class="d-flex justify-content-between align-items-center">
                                        <span><i class="far fa-calendar"></i>Күні </span>
                                        {{ date('d.m.Y', strtotime($item->date)) }}
                                    </div>
                                </li>
                            </ul>

                            <div class="buy-btn">

                            </div>
                        </div>
                    </div>
                </div>
                <!-- Course Sidebar end -->
            </div>
        </div>
    </section>
    <section class="contact " style="padding-bottom: 100px">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-11 col-lg-11">
                    <div style="
                        text-align: center;
                        margin: auto;
                        width: 100%;
                        padding: 10px;
                    ">
                        <span style="
                            background-color: #4267B2;
                            color: #fff;
                            padding: 10px 17px;
                            border-radius: 20px;
                        "> 1</span>
                            <span style="color: #4267B2">-------------------------------------</span>
                        <span style="
                            background-color: #4267B2;
                            color: #fff;
                            padding: 10px 17px;
                            border-radius: 20px;
                        ">
                            2</span>
                            <br>
                            <br>
                        <span style="color: #4267B2; font-family: var(--bs-font-sans-serif) !important;">Тіркелу</span>
                             <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                        <span style="color: #4267B2; font-family: var(--bs-font-sans-serif) !important;">Сұраныс</span>
                    </div>
                    @if(Cookie::get('tid'))
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">

                                    <input type="text" id="name" name="name" class="form-control" placeholder="Аты">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Телефон">
                                </div>
                            </div>
                            <input type="hidden" name="translation_id" id="translation_id" class="form-control" placeholder="ID" value="{{ $item->id }}">

                        </div>

                        <div class="col-lg-12">
                            <div class="text-center">
                                    <button onclick="CallBack()"  class="btn btn-main w-100 rounded" type="submit">Жіберу</button>
                            </div>
                        </div>
                    @else
                        <div style="text-align: center">
                            <script async src="https://telegram.org/js/telegram-widget.js?21" data-telegram-login="live_fatiha_bot" data-size="medium" data-radius="8" data-auth-url="{{ route('site.translation.show', $item->id) }}" data-request-access="write"></script>
                        </div>
                    @endif
                    <br>
                    <a class="btn btn-main w-100 rounded" style="background-color: #25D366; border: none" href="https://wa.me/77476275163?text=Сәлеметсіз%20бе.%20Мастер%20класс%20туралы%20білейін%20деген%20едім." data-action="share/whatsapp/share" target="_blank"><img src="https://cdn-icons-png.flaticon.com/512/3536/3536445.png" width="15px" height="15px" alt="">
                        менеджерге жазу
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact section End -->


    <div class="modal fade" id="callModal" tabindex="-1" aria-labelledby="callexampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label for="link-ref" style="text-align: center" class="col-form-label">  Бізбен хабарласқаныңызға рахмет, біз сізбен жақын арада хабарласамыз!</label>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="errorModal" tabindex="-1" aria-labelledby="errorModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label for="link-ref" style="text-align: center" class="col-form-label">  Бірдеңе дұрыс болмады, кейінірек қайталап көріңіз</label>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="validate" tabindex="-1" aria-labelledby="validate" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button  type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <label for="link-ref" style="text-align: center" class="col-form-label">  Аты, телефон толтырыңыз</label>
                </div>
            </div>
        </div>
    </div>

    <script>

        function  CallBack(){
            let name = document.getElementById('name').value;
            let phone = document.getElementById('phone').value;
            let translation_id = document.getElementById('translation_id').value;

            if( name && phone)
                $.post("{{ route('admin.translation_request.store') }}",
                    { name: name, phone: phone, translation_id: translation_id }
                ).then(function (response) {
                    document.getElementById('name').value = "";
                    document.getElementById('phone').value = "";
                    if (response.code === 200){
                        $('#callModal').modal('show');
                        window.location= 'https://wa.me/77476275163?text=Сәлеметсіз%20бе.%20Мастер%20класс%20туралы%20білейін%20деген%20едім.'
                    } else
                    {
                        $('#errorModal').modal('show');
                    }

                });
            else
                $('#validate').modal('show');
        }
    </script>
    <style>
        .galym-page-header {
            padding: 90px 0px;
            background: #F4F5F8;
            position: relative;
            background: url(../images/bg/page-title.jpg);
            background-repeat: no-repeat;
            background-size: cover;
            z-index: 1;
        }
        .galym-page-header:after {
            position: absolute;
            content: "";
            left: 0px;
            top: 0px;
            width: 100%;
            height: 100%;
            z-index: -1;
        }
        .page-header-2:before {
            background: none;
            }
</style>
@endsection