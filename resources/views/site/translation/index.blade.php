@extends('layouts.site')
@section('title', '- '  )

@section('content')
<section class="about-3 section-padding">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-xl-5 col-lg-6">
                <div class="about-img">
                    <img src="assets/img/img_10.png" alt="" class="img-fluid">
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about-content mt-5 mt-lg-0">
                    <div class="heading mb-50">
                        <h2 class="font-lg">...........................</h2>
                    </div>

                    <div class="about-features">
                        <div class="feature-item feature-style-left">
                            <div class="feature-icon icon-bg-3 icon-radius">
                                <i class="fa fa-video"></i>
                            </div>
                            <div class="feature-text">
                                <h4>...............</h4>
                                <p>......................................</p>
                            </div>
                        </div>

                        <div class="feature-item feature-style-left">
                            <div class="feature-icon icon-bg-2 icon-radius">
                                <i class="far fa-file-certificate"></i>
                            </div>
                            <div class="feature-text">
                                <h4>.............</h4>
                                <p>..........................</p>
                            </div>
                        </div>

                        <div class="feature-item feature-style-left">
                            <div class="feature-icon icon-bg-1 icon-radius">
                                <i class="fad fa-users"></i>
                            </div>
                            <div class="feature-text">
                                <h4>..............</h4>
                                <p>.................................</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="section-padding pt-0">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="heading mb-50 text-center">
                    <h2>....................</h2>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            @foreach($translations as $item)
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="course-grid course-style-3">
                        <div class="course-header">
                            <div class="course-thumb">
                                <a href="{{ route('site.translation.show', $item->id) }}">
                                    <img src="/{{ $item->image }}" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>

                        <div class="course-content">
                            <h3 class="course-title mb-20">
                                <a href="{{ route('site.translation.show', $item->id) }}">
                                    {{ $item->name }}
                                </a>
                            </h3>

                            <div class="course-footer mt-20 d-flex align-items-center justify-content-between">
                                <div class="course-price">
                                    <a style="font-family: var(--bs-font-sans-serif); !important;" href='{{ route('site.translation.show', $item->id) }}'>
                                        {{ $item->price ? 'Ақылы' : 'Тегін' }}
                                    </a>
                                </div>
                                <a href='{{ route('site.translation.show', $item->id) }}' style="font-family: var(--bs-font-sans-serif); !important;"
                                   class="btn btn-main-outline btn-radius btn-sm"> Толығырақ  <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
</section>
@endsection