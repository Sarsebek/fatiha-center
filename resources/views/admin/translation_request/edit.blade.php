@extends('layouts.main')
@section('title', '- ' . $title ?? '')

@section('content')

    <div class="col-12">
        @include('partials.messages')

            <form action="{{ route($namespace_update, $item->id) }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="box-header with-border">
                    <div class="mr-auto">
                        <div class="d-inline-block align-items-center">
                            <nav>
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route($namespace_index) }}"><i class="mdi mdi-home-outline"></i></a></li>
                                    <li class="breadcrumb-item" aria-current="page">{{ $title ?? '' }}</li>
                                    <li class="breadcrumb-item active" aria-current="page">Редактирование</li>
                                </ol>
                            </nav>
                            <a type="button" href="{{ url()->previous() }}" class="btn btn-info mb-5">Назад</a>
                            <button type="submit" class="btn btn-success mb-5">Сохранить и выйти</button>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Название</label>
                        <div class="col-md-10">
                            <input value="{{ old('name') ?? $item->name }}" class="form-control" type="text" name="name" placeholder="Название">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Описание</label>
                        <div class="col-md-10">
                            <textarea  class="form-control" type="text" name="description" placeholder="Описание">{{ old('description') ?? $item->description }}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Язык</label>
                        <div class="col-md-10">
                            <input value="{{ old('language') ?? $item->language }}" class="form-control" type="text" name="language" placeholder="Язык">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Ссылка</label>
                        <div class="col-md-10">
                            <input value="{{ old('link') ?? $item->link }}" class="form-control" type="text" name="link" placeholder="Ссылка">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Дата</label>
                        <div class="col-md-10">
                            <input value="{{ old('date') ?? $item->date }}" class="form-control" type="date" name="date" placeholder="Дата">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-form-label col-md-2">Время</label>
                        <div class="col-md-10">
                            <input value="{{ old('time') ?? $item->time }}" class="form-control" type="time" name="time" placeholder="Время">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="price" class="col-form-label col-md-2">Цена(если отмечено то платно)</label>
                        <div class="col-md-10">
                            <input id="price" style="left: 15px; opacity: 1; width: 40px; height: 40px;" @if($item->price) checked @endif  type="checkbox" name="price">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="image" class="col-form-label col-md-2">Фото (загрузите для обновления)</label>
                        <div class="col-md-10">
                            <input id="image"  type="file" name="image">
                            @if($item->image)
                                <a href="/{{ $item->image }}" target="_blank">
                                    <img src="/{{ $item->image }}" alt="image" width="100px" height="70px">
                                </a>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="background" class="col-form-label col-md-2">Баннер (загрузите для обновления)</label>
                        <div class="col-md-10">
                            <input id="background"  type="file" name="background">
                            @if($item->background)
                                <a href="/{{ $item->background }}" target="_blank">
                                    <img src="/{{ $item->background }}" alt="background" width="100px" height="70px">
                                </a>
                            @endif
                        </div>

                    </div>

                </div>
            </form>

        </div>

        <!-- /.box -->
    </div>


@endsection
@push('footer_scripts')
    <script type="text/javascript" src="/js/jquery.mask.js"></script>
@endpush