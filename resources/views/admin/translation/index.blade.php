@extends('layouts.main')
@section('title', '- ' . $title ?? '')

@section('content')

    <div class="col-12">
        @include('partials.messages')

        <div class="box">
            <div class="box-header with-border">
                <div class="mr-auto">
                    <div class="d-inline-block align-items-center">
                        <nav>
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route($controller_name) }}"><i class="mdi mdi-home-outline"></i></a></li>
                                <li class="breadcrumb-item" aria-current="page">{{ $title ?? '' }}</li>
                                <li class="breadcrumb-item active" aria-current="page">Просмотр</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Дата</th>
                                <th>Время</th>
                                <th>Ссылка</th>
                                <th>Цена</th>
                                <th>Язык</th>
                                <th>Фото</th>
                                <th>Баннер</th>
                                <th>Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($items as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ substr($item->description, 0, 50) }}...</td>
                                    <td>{{ $item->date }}</td>
                                    <td>{{ \Carbon\Carbon::createFromFormat('H:i:s', $item->time)->format('H:i') }}</td>
                                    <td>

                                        <a target="_blank" href="{{ strpos($item->link, 'https://') !== false ? url($item->link) : url('https://' . $item->link) }}">{{ $item->link }}</a>
                                    </td>
                                    <td>
                                        {{ $item->price ? 'Платно' : 'Бесплатно' }}
                                    </td>
                                    <td>
                                        {{ $item->language }}
                                    </td>
                                    <td>
                                        @if($item->image)
                                            <a href="/{{ $item->image }}">
                                                <img src="/{{ $item->image }}" alt="image" width="70px" height="70px">
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        @if($item->background)
                                            <a href="/{{ $item->background }}">
                                                <img src="/{{ $item->background }}" alt="background" width="70px" height="70px">
                                            </a>
                                        @endif
                                    </td>
                                    <td style="font-size:17px;">
                                        <a class="btn btn-rounded" href="{{ route($namespace_edit, $item->id) }}"><span class="fa fa-pencil"></span></a>
                                        @include('partials.delete')
                                    </td>
                                </tr>
                            @empty
                            @endforelse
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>ID</th>
                                <th>Название</th>
                                <th>Описание</th>
                                <th>Дата</th>
                                <th>Время</th>
                                <th>Ссылка</th>
                                <th>Цена</th>
                                <th>Язык</th>
                                <th>Фото</th>
                                <th>Баннер</th>
                                <th>Действия</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="dt-buttons btn-group">
                    <a href="{{ route($namespace_create) }}" class="btn btn-secondary buttons-copy buttons-html5" tabindex="0">
                        <span>Добавить трансляцию</span>
                    </a>
                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>


@endsection